import 'package:flutter/material.dart';

TextTheme getAppBarTextTheme() {
  return TextTheme(
    headline6: TextStyle(
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.bold,
    ),
  );
}
