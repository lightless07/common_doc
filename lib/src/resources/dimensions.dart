import 'package:flutter/material.dart';

const EdgeInsets dimLR36T16B0 = EdgeInsets.fromLTRB(36.0, 8.0, 36.0, 0.0);
const EdgeInsets dimLR36T8B0 = EdgeInsets.fromLTRB(36.0, 8.0, 36.0, 0.0);
const EdgeInsets dimLR36T8B15 = EdgeInsets.fromLTRB(36.0, 8.0, 36.0, 15.0);
const EdgeInsets dimLR80TB16 = EdgeInsets.fromLTRB(80.0, 16.0, 80.0, 16.0);
const EdgeInsets dimLR36TB20 = EdgeInsets.fromLTRB(36.0, 20.0, 36.0, 20.0);
const EdgeInsets dimLR3TB0 = EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0);
const double dimCoverHeight = 350.0;
const double dimSpace5 = 5.0;
const double dimSpace10 = 10.0;
const double dimSpace20 = 20.0;
const double appBarBottomBtnPosition = 15.0;
