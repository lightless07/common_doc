import 'package:flutter/material.dart';

IconButton loadBackButtonAppBar(BuildContext context) {
  return IconButton(
    icon: Image.asset('assets/images/app_bar_back_icon.png'),
    tooltip: "Regresar",
    onPressed: () => Navigator.of(context).pop(),
  );
}

Text loadBackTitleAppBar() {
  return Text(
    "Regresar",
    textDirection: TextDirection.ltr,
    style: TextStyle(
        color: Colors.white, fontWeight: FontWeight.normal, fontSize: 14.0),
  );
}
