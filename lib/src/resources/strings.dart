class Strings {
  ///Home
  static const String selectNewProfileTitle = "Esta cita es para";

  ///Misc
  static const String hintDropDown = "Selecciona una opción";

  /// Loading
  static const String titleLoadingDialog = "Cargando";

  /// FlushBar
  static const String titleErrorFlushBar = "Atención";
  static const String messageLoginErrorFlushBar =
      "Error al tratar de iniciar sesión";
  static const String messageNoInternetErrorFlushBar =
      "El dispositivo no puede acceder a internet.";
}
