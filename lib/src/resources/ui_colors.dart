import 'package:flutter/material.dart';

class UIColors {
  static const Color blue = Color(0xFF0000FF);
  static const Color black = Color(0xFF000000);
  static const Color white = Color(0xFFFFFFFF);
  static const Color white80 = Color(0xCCFFFFFF);
  static const Color white92 = Color(0xEBFFFFFF);
  static const Color green = Color(0xFF00FF00);
  static const Color yellow = Color(0xFFFFFF00);
  static const Color red = Color(0xFFFF0000);
  static const Color grey = Color(0xFF808080);
  static const Color black54 = Color(0xFF474f54);
  static const Color transparent = Color(0x00FFFFFF);
  static const Color teal = Color(0x00008080);
  static const Color transparentBlue = Color(0x0D725ac1);

  static const Color editTextColor = Color(0xFFf9f9f9);
  static const Color startDegradedColor = Color(0xff374ABE);
  static const Color endDegradedColor = Color(0xff64B6FF);
  static const Color topAppBarDegraded = Color(0xFFe1e1e1);
  static const Color bottomAppBarDegraded = Color(0xFFFFFFFF);

  static const Color editTextFontColor = Color(0xFF242038);
  static const Color editTextBorderColor = Color(0xFFf0f0f0);
  static const Color editTextBackgroundColor = Color(0xFFfafafa);

  static const Color mainTextFontColor = Color(0xFF242038);
  static const Color secondTextFontColor = Color(0xFF725ac1);
  static const Color thirdTextFontColor = Color(0xFFa7a7a7);
  static const Color blogTitleTextFontColor = Color(0xFF352272);

  static const Color tabUnSelectedFontColor = Color(0xFF777777);
  static const Color tabSelectedFontColor = Color(0xFFFFFFFF);
  static const Color tabBackgroundColor = Color(0xFFe8e8e8);

  static const Color customDropDownBorderColor = Color(0xFFf0f0f0);
  static const Color customDropDownBackgroundColor = Color(0xFFfafafa);

  static const Color forgotPasswordFontColor = Color(0xFF242038);
  static const Color appBarBackgroundColor = Color(0xFFd66d75);

  static const Color tabBarSelectedColor = Color(0xFFd66d75);
  static const Color tabBarDefaultColor = Color(0xFF725ac1);

  static const Color disabledButtonBackgroundColor = Color(0xFFe0e0e0);
  static const Color disabledButtonFontColor = Color(0xFF777777);

  static const Color checkBoxAbleColor = Color(0xFF725ac1);
  static const Color checkBoxDisabledColor = Color(0xFFf0f0f0);

  static const Color calendarBackgroundColor = Color(0xFFf8f5ff);
  static const Color calendarSelectedTileBgColor = Color(0xFF725ac1);
  static const Color calendarUnableTileBgColor = Color(0xFFe0e0e0);
  static const Color calendarAbleTileBgColor = Color(0xFFFFFFFF);
  static const Color calendarSelectedTileFontColor = Color(0xFFFFFFFF);
  static const Color calendarUnableTileFontColor = Color(0xFF777777);
  static const Color calendarAbleTileFontColor = Color(0xFF725ac1);
  static const Color calendarSelectedTileBorderColor = Color(0x00725ac1);
  static const Color calendarUnableTileBorderColor = Color(0xFFe0e0e0);
  static const Color calendarAbleTileBorderColor = Color(0xFF725ac1);

  static const Color profileMainDataContainerStartDegradedColor =
      Color(0xFFFFFFFF);
  static const Color profileMainDataContainerEndDegradedColor =
      Color(0xFFf7f7f7);
  static const Color profileMainDataContainerBgColor = Color(0xFFd48d90);

  static const Color blueButtonStartDegradedColor = Color(0xFF725ac1);
  static const Color blueButtonEndDegradedColor = Color(0xFFa48def);
  static const Color blueDividerColor = Color(0xFF725ac1);
  static const Color lightGrayDividerColor = Color(0xFFb4b4b4);
  static const Color grayDividerColor = Color(0xFF707070);
  static const Color roundedButtonBorderColor = Color(0xFF725ac1);
  static const Color roundedButtonBackgroundColor = Color(0xFF725ac1);
  static const Color roundedButtonFontColor = Color(0xFF725ac1);

  static const Color roundedWhiteBlackButtonFontColor = Color(0xFFd66d75);
  static const Color roundedWhiteBlackButtonBackgroundColor = Color(0xFFFFFFFF);
  static const Color roundedWhiteBlackButtonBorderColor = Color(0xFFd66d75);
}
