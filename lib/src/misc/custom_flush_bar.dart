import 'package:another_flushbar/flushbar.dart';
import 'package:common_ui/src/resources/strings.dart';
import 'package:flutter/material.dart';

Widget getInternetErrorFlushBar(BuildContext context) {
  return Flushbar(
    title: Strings.titleErrorFlushBar,
    message: Strings.messageNoInternetErrorFlushBar,
    backgroundColor: Colors.red,
    duration: Duration(seconds: 3),
    isDismissible: false,
    icon: Icon(
      Icons.wifi_off,
      color: Colors.white,
    ),
    boxShadows: [
      BoxShadow(
        color: Colors.red[800]!,
        offset: Offset(0.0, 2.0),
        blurRadius: 3.0,
      )
    ],
  )..show(context);
}

Widget getWSErrorFlushBar(BuildContext context, String title, String message) {
  return Flushbar(
    title: title,
    message: message,
    backgroundColor: Colors.red,
    duration: Duration(seconds: 3),
    isDismissible: false,
    icon: Icon(
      Icons.info_outline,
      color: Colors.white,
    ),
    boxShadows: [
      BoxShadow(
        color: Colors.red[800]!,
        offset: Offset(0.0, 2.0),
        blurRadius: 3.0,
      )
    ],
  )..show(context);
}

Widget getGeneralErrorFlushBar(
    BuildContext context, String title, String message) {
  return Flushbar(
    title: title,
    message: message,
    backgroundColor: Colors.red,
    duration: Duration(seconds: 3),
    isDismissible: false,
    icon: Icon(
      Icons.info_outline,
      color: Colors.white,
    ),
    boxShadows: [
      BoxShadow(
        color: Colors.red[800]!,
        offset: Offset(0.0, 2.0),
        blurRadius: 3.0,
      )
    ],
  )..show(context);
}
