import 'package:common_ui/src/resources/strings.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';

bool validateEmail(TextEditingController emailController) {
  if (emailController.text.isEmpty) {
    return false;
  } else {
    if (EmailValidator.validate(emailController.text)) {
      return true;
    } else {
      return false;
    }
  }
}

bool validateFirstNames(TextEditingController nameController) {
  final RegExp validNameCharacters = RegExp(r'^[a-zA-Z]+$');
  if (nameController.text.isNotEmpty &&
      validNameCharacters.hasMatch(nameController.text)) {
    return true;
  } else {
    return false;
  }
}

bool validateSecondNames(TextEditingController nameController) {
  final RegExp validNameCharacters = RegExp(r'^[a-zA-Z]+$');
  if (nameController.text.isEmpty) {
    return true;
  } else {
    if (validNameCharacters.hasMatch(nameController.text)) {
      return true;
    } else {
      return false;
    }
  }
}

bool validatePlainText(String plainText) {
  final RegExp validCharacters = RegExp(r'^[a-zA-Z]+$');
  if (plainText.isNotEmpty &&
      validCharacters.hasMatch(plainText)) {
    return true;
  } else {
    return false;
  }
}

bool validateDropDown(String plainText) {
  if (plainText != Strings.hintDropDown) {
    return true;
  } else {
    return false;
  }
}

bool validatePassword(TextEditingController passwordController) {
  final RegExp passwordCharactersAllowed = RegExp(r'^[a-zA-Z0-9_\-=@,\.;]+$');
  if (passwordController.text.isNotEmpty &&
      passwordCharactersAllowed.hasMatch(passwordController.text)) {
    return true;
  } else {
    return false;
  }
}

bool validateCommonText(TextEditingController editingController) {
  final RegExp passwordCharactersAllowed = RegExp(r'^[a-zA-Z0-9\,.# ]+$');
  if (editingController.text.isNotEmpty &&
      passwordCharactersAllowed.hasMatch(editingController.text)) {
    return true;
  } else {
    return false;
  }
}

bool validateCommonString(String text) {
  final RegExp passwordCharactersAllowed = RegExp(r'^[a-zA-Z0-9\,.# ]+$');
  if (text.isNotEmpty && passwordCharactersAllowed.hasMatch(text)) {
    return true;
  } else {
    return false;
  }
}

bool isFieldEmpty(TextEditingController textEditingController) {
  return textEditingController.text.isEmpty;
}

bool validEqualPasswords(TextEditingController passwordController,
    TextEditingController confirmPasswordController) {
  if (passwordController.text == confirmPasswordController.text) {
    return true;
  } else {
    return false;
  }
}
