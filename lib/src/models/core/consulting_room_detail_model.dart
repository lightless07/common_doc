// To parse this JSON data, do
//
//     final consultingRoomDetailModel = consultingRoomDetailModelFromJson(jsonString);

import 'dart:convert';

ConsultingRoomDetailModel consultingRoomDetailModelFromJson(String str) =>
    ConsultingRoomDetailModel.fromJson(json.decode(str));

String consultingRoomDetailModelToJson(ConsultingRoomDetailModel data) =>
    json.encode(data.toJson());

class ConsultingRoomDetailModel {
  ConsultingRoomDetailModel({
    this.calendar,
    this.amount,
    this.location,
    this.consultingRoomName,
  });

  factory ConsultingRoomDetailModel.fromJson(Map<String, dynamic> json) =>
      ConsultingRoomDetailModel(
        calendar: List<Calendar>.from(
            json["calendar"].map((x) => Calendar.fromJson(x))),
        amount: json["amount"],
        location: Location.fromJson(json["location"]),
        consultingRoomName: json["consultingRoomName"],
      );

  List<Calendar>? calendar;
  String? amount;
  Location? location;
  String? consultingRoomName;

  Map<String, dynamic> toJson() => {
        "calendar": List<dynamic>.from(calendar!.map((x) => x.toJson())),
        "amount": amount,
        "location": location!.toJson(),
        "consultingRoomName": consultingRoomName,
      };
}

class Calendar {
  Calendar({
    this.weekday,
    this.day,
    this.month,
    this.able,
    this.capacity,
    this.schedule,
  });

  factory Calendar.fromJson(Map<String, dynamic> json) => Calendar(
        weekday: json["weekday"],
        day: json["day"],
        month: json["month"],
        able: json["able"],
        capacity: json["capacity"],
        schedule: List<String>.from(json["schedule"].map((x) => x)),
      );

  String? weekday;
  int? day;
  String? month;
  bool? able;
  int? capacity;
  List<String>? schedule;

  Map<String, dynamic> toJson() => {
        "weekday": weekday,
        "day": day,
        "month": month,
        "able": able,
        "capacity": capacity,
        "schedule": List<dynamic>.from(schedule!.map((x) => x)),
      };
}

class Location {
  Location({
    this.latitude,
    this.longitude,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        latitude: json["latitude"],
        longitude: json["longitude"],
      );

  Map<String, dynamic> toJson() => {
        "latitude": latitude,
        "longitude": longitude,
      };

  double? latitude;
  double? longitude;
}

class EnumValues<T> {
  EnumValues(this.map);

  Map<String, T> map;
  Map<T?, String>? reverseMap;

  Map<T?, String>? get reverse {
    reverseMap ??= map.map((String k, Object? v) => MapEntry(v as T?, k));
    return reverseMap;
  }
}
