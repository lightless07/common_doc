class TreatmentIndication {
  TreatmentIndication(
    this.name,
    this.type,
    this.frequencyDetail,
    this.shotsTaken,
    this.totalShots,
  );

  final String name;
  final String type;
  final String frequencyDetail;
  final int shotsTaken;
  final int totalShots;
}
