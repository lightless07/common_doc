class UserInfoList {
  UserInfoList({
    this.photos,
  });

  factory UserInfoList.fromJson(List<dynamic> parsedJson) {
    List<UserInfo> photos = <UserInfo>[];
    photos = parsedJson.map((i) => UserInfo.fromJson(i)).toList();
    return UserInfoList(photos: photos);
  }

  final List<UserInfo>? photos;
}

class UserInfo {
  UserInfo({
    this.userId,
    this.id,
    this.title,
    this.body,
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) {
    return UserInfo(
      userId: json['userId'].toString(),
      id: json['id'].toString(),
      title: json['title'],
      body: json['body'],
    );
  }

  String? userId;
  String? id;
  String? title;
  String? body;
}
