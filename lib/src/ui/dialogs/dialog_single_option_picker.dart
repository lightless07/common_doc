import 'package:flutter/material.dart';

class DialogSingleOptionPicker extends StatefulWidget {
  DialogSingleOptionPicker(
      {required this.options,
      required this.currentOption,
      required this.title,
      required this.onDataChanged});

  final String title;
  final int currentOption;
  final List<String> options;
  final Function onDataChanged;

  @override
  DialogSingleOptionPickerState createState() =>
      DialogSingleOptionPickerState();
}

class DialogSingleOptionPickerState extends State<DialogSingleOptionPicker> {
  int? selectedIndex;

  @override
  void initState() {
    selectedIndex = widget.currentOption;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: EdgeInsets.all(15),
      content: Container(
          width: double.maxFinite,
          child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
            ConstrainedBox(
              constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height * 0.4,
              ),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: widget.options.length,
                  itemBuilder: (BuildContext context, int index) {
                    return RadioListTile(
                        title: Text(widget.options[index]),
                        value: index,
                        groupValue: selectedIndex,
                        onChanged: (int? value) {
                          setState(() {
                            selectedIndex = index;
                          });
                        });
                  }),
            )
          ])),
      actions: <Widget>[
        TextButton(
            child: const Text('Cancelar'),
            onPressed: () {
              Navigator.pop(context);
            }),
        TextButton(
            child: const Text('Aceptar'),
            onPressed: () {
              widget.onDataChanged(selectedIndex);
              Navigator.pop(context);
              /*if (widget.currentOption == selectedIndex) {
                Navigator.pop(context);
              } else {
                widget.onDataChanged(selectedIndex);
                Navigator.pop(context);
              }*/
            })
      ],
    );
  }

  void onChangedValue(int onChangedIndex) {
    selectedIndex = onChangedIndex;
  }
}
