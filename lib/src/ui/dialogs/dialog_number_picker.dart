import 'package:flutter/material.dart';
import 'package:flutter_spinbox/flutter_spinbox.dart';

class DialogNumberPicker extends StatefulWidget {
  DialogNumberPicker(
      {required this.currentValue,
      required this.title,
      required this.onDataChanged});

  final String title;
  final double currentValue;
  final Function onDataChanged;

  @override
  DialogNumberPickerState createState() => DialogNumberPickerState();
}

class DialogNumberPickerState extends State<DialogNumberPicker> {
  double? newValue;

  bool flag = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: EdgeInsets.all(15),
      content: Container(
        height: 100,
        child: CupertinoSpinBox(
          min: 1,
          max: 100,
          value: 50,
          onChanged: (double value) {
            onChangedValue(value);
          },
          prefix: Text("Peso"),
          suffix: Text("kg"),
        ),
      ),
      actions: <Widget>[
        TextButton(
            child: const Text('Cancelar'),
            onPressed: () {
              Navigator.pop(context);
            }),
        TextButton(
            child: const Text('Aceptar'),
            onPressed: () {
              if (widget.currentValue == newValue) {
                Navigator.pop(context);
              } else {
                widget.onDataChanged(newValue);
                Navigator.pop(context);
              }
            })
      ],
    );
  }

  void onChangedValue(double onChangedValue) {
    newValue = onChangedValue;
  }
}
