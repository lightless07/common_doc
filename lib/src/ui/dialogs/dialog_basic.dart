import 'package:flutter/material.dart';

class DialogBasic extends StatefulWidget {
  DialogBasic({
    required this.message,
    required this.title,
    required this.isSingleAction,
    this.onAcceptClick,
    this.onCancelClick,
    this.onContinueClick,
  });

  final String title;
  final String message;
  final bool isSingleAction;
  final Function? onAcceptClick;
  final Function? onCancelClick;
  final Function? onContinueClick;

  @override
  DialogBasicState createState() => DialogBasicState();
}

class DialogBasicState extends State<DialogBasic> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: EdgeInsets.all(15),
      content: Row(
        children: <Widget>[
          Expanded(
            child: Text(widget.message),
          )
        ],
      ),
      actions: <Widget>[
        !widget.isSingleAction
            ? TextButton(
                child: const Text('Cancelar'),
                onPressed: () {
                  Navigator.pop(context);
                  widget.onCancelClick!();
                })
            : SizedBox(),
        !widget.isSingleAction
            ? TextButton(
                child: const Text('Aceptar'),
                onPressed: () {
                  Navigator.pop(context);
                  widget.onAcceptClick!();
                })
            : SizedBox(),
        widget.isSingleAction
            ? TextButton(
                child: const Text('Continuar'),
                onPressed: () {
                  Navigator.pop(context);
                  widget.onContinueClick!();
                })
            : SizedBox(),
      ],
    );
  }
}
