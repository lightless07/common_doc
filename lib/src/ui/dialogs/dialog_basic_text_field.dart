import 'package:flutter/material.dart';

class DialogBasicTextField extends StatefulWidget {
  DialogBasicTextField(
      {required this.currentText,
      required this.title,
      required this.onDataChanged});

  final String title;
  final String currentText;
  final Function onDataChanged;

  @override
  DialogBasicTextFieldState createState() => DialogBasicTextFieldState();
}

class DialogBasicTextFieldState extends State<DialogBasicTextField> {
  TextEditingController textFieldController = TextEditingController();
  String? newText;

  bool flag = true;

  @override
  void initState() {
    super.initState();
    textFieldController.text = widget.currentText;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(widget.title),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
      insetPadding: EdgeInsets.all(15),
      content: Row(
        children: <Widget>[
          Expanded(
              child: Container(
            child: TextField(
              controller: textFieldController,
              autofocus: true,
              keyboardType: TextInputType.multiline,
              maxLines: null,
              decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 8),
                  isCollapsed: false,
                  hintText: 'Ingrese la información'),
            ),
          ))
        ],
      ),
      actions: <Widget>[
        TextButton(
            child: const Text('Cancelar'),
            onPressed: () {
              Navigator.pop(context);
            }),
        TextButton(
            child: const Text('Aceptar'),
            onPressed: () {
              if (widget.currentText == textFieldController.text) {
                Navigator.pop(context);
              } else {
                widget.onDataChanged(textFieldController.text);
                Navigator.pop(context);
              }
            })
      ],
    );
  }
}
