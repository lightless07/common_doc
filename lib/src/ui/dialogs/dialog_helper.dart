import 'package:common_ui/src/resources/strings.dart';
import 'package:flutter/material.dart';

showLoadingDialog(BuildContext context) {
  AlertDialog alert = AlertDialog(
    content: Row(
      children: <Widget>[
        CircularProgressIndicator(),
        SizedBox(
          height: 10,
          width: 10,
        ),
        Container(
            margin: EdgeInsets.only(left: 5),
            child: Text(Strings.titleLoadingDialog)),
      ],
    ),
  );
  showDialog(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}

Widget showLoadingIndicator(bool isLoading) {
  return isLoading
      ? Container(
          color: Colors.grey[300],
          width: 70.0,
          height: 70.0,
          child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Center(child: CircularProgressIndicator())),
        )
      : Container();
}

Widget setupListDialogContainer({required List<String> list, Function? onClick}) {
  return Container(
    //height: 300.0, // Change as per your requirement
    width: 300.0, // Change as per your requirement
    child: ListView.builder(
      shrinkWrap: true,
      itemCount: list.length,
      itemBuilder: (BuildContext context, int index) {
        return Column(
          children: <Widget>[
            ListTile(
              title: Text(list[index]),
              onTap: () => onClick!(index),
            ),
            Divider(
              color: Colors.black,
            ), //                           <-- Divider
          ],
        );
      },
    ),
  );
}
