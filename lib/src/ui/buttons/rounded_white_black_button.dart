import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class RoundedWhiteBlackButton extends StatefulWidget {
  const RoundedWhiteBlackButton(
      {Key? key,
      this.title,
      this.isIcon,
      this.iconAsset,
      this.padding,
      this.onClick})
      : super(key: key);

  final String? title;
  final bool? isIcon;
  final IconData? iconAsset;
  final EdgeInsets? padding;
  final Function? onClick;

  @override
  RoundedWhiteBlackButtonState createState() => RoundedWhiteBlackButtonState();
}

class RoundedWhiteBlackButtonState extends State<RoundedWhiteBlackButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      color: UIColors.roundedWhiteBlackButtonBackgroundColor,
      padding: EdgeInsets.fromLTRB(widget.padding!.left, widget.padding!.top,
          widget.padding!.right, widget.padding!.bottom),
      child: OutlinedButton(
        onPressed: () {
          widget.onClick!();
        },
        style: OutlinedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          side: BorderSide(color: UIColors.roundedWhiteBlackButtonBorderColor),
        ),
        child: Stack(
          children: <Widget>[
            widget.isIcon!
                ? Align(
                    alignment: Alignment.centerLeft,
                    child: Icon(
                      widget.iconAsset,
                      color: UIColors.roundedWhiteBlackButtonFontColor,
                    ))
                : Container(),
            Align(
                alignment: Alignment.center,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    widget.title!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: UIColors.roundedWhiteBlackButtonFontColor,
                        fontWeight: FontWeight.normal),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
