import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class BlueButton extends StatefulWidget {
  const BlueButton({Key? key, this.title, this.isDisabled, this.onClick})
      : super(key: key);

  final String? title;
  final bool? isDisabled;
  final Function? onClick;

  @override
  BlueButtonState createState() => BlueButtonState();
}

class BlueButtonState extends State<BlueButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        padding: EdgeInsets.all(5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(90.0)),
            color: widget.isDisabled!
                ? UIColors.disabledButtonBackgroundColor
                : UIColors.tabBarDefaultColor),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                widget.title!,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: widget.isDisabled!
                        ? UIColors.disabledButtonFontColor
                        : UIColors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
            ]),
      ),
      onTap: () {
        widget.onClick!();
      },
    );
  }
}
