import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class RoundedWhiteButton extends StatefulWidget {
  const RoundedWhiteButton(
      {Key? key,
      this.title,
      this.isIcon,
      this.iconAsset,
      this.padding,
      this.onClick})
      : super(key: key);

  final String? title;
  final bool? isIcon;
  final IconData? iconAsset;
  final EdgeInsets? padding;
  final Function? onClick;

  @override
  RoundedWhiteButtonState createState() => RoundedWhiteButtonState();
}

class RoundedWhiteButtonState extends State<RoundedWhiteButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      padding: EdgeInsets.fromLTRB(widget.padding!.left, widget.padding!.top,
          widget.padding!.right, widget.padding!.bottom),
      child: OutlinedButton(
        onPressed: () {
          widget.onClick!();
        },
        style: OutlinedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          side: BorderSide(color: UIColors.roundedButtonBorderColor),
        ),
        child: Stack(
          children: <Widget>[
            widget.isIcon!
                ? Align(
                    alignment: Alignment.centerLeft,
                    child: Icon(
                      widget.iconAsset,
                      color: UIColors.roundedButtonFontColor,
                    ))
                : Container(),
            Align(
                alignment: Alignment.center,
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    widget.title!,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: UIColors.roundedButtonFontColor,
                        fontWeight: FontWeight.normal),
                  ),
                ))
          ],
        ),
      ),
    );
  }
}
