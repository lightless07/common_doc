import 'package:common_ui/src/enums/enum_type_text_fields.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CustomEditTextOverlappedButton extends StatefulWidget {
  const CustomEditTextOverlappedButton(
      {Key? key,
      this.myController,
      this.type,
      this.title,
      this.hint,
      this.error,
      this.isInputValid,
      this.padding,
      this.onClick})
      : super(key: key);

  final TextEditingController? myController;
  final String? title;
  final String? hint;
  final String? error;
  final EdgeInsets? padding;
  final TextFieldType? type;
  final Function? onClick;
  final bool? isInputValid;

  @override
  CustomEditTextOverlappedButtonState createState() =>
      CustomEditTextOverlappedButtonState();
}

class CustomEditTextOverlappedButtonState
    extends State<CustomEditTextOverlappedButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.fromLTRB(widget.padding!.left,
            widget.padding!.top, widget.padding!.right, widget.padding!.bottom),
        child: Column(children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(widget.title!,
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 14.0,
                    color: UIColors.editTextFontColor,
                    fontWeight: FontWeight.normal)),
            padding: EdgeInsets.fromLTRB(
                widget.padding!.left,
                widget.padding!.top,
                widget.padding!.right,
                widget.padding!.bottom),
          ),
          SizedBox(height: 5),
          Stack(
            //alignment:new Alignment(x, y)
            children: <Widget>[
              TextField(
                controller: widget.myController,
                onEditingComplete: () => node.nextFocus(),
                // Move focus to next
                keyboardType: getKeyboardType(),
                obscureText: inputIsPassword(),
                style: TextStyle(
                    fontSize: 14.0,
                    color: UIColors.editTextFontColor,
                    fontWeight: FontWeight.bold),
                decoration: InputDecoration(
                  /*prefixIcon: Icon(
                getIcon(),
                  color: UIColors.editTextBorderColor
              ),*/
                  border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(90.0),
                    ),
                    borderSide: BorderSide(
                      color: UIColors.editTextBorderColor,
                    ),
                  ),
                  isDense: true,
                  contentPadding:
                      EdgeInsets.only(left: 32, bottom: 16, top: 16, right: 32),
                  filled: true,
                  hintStyle: TextStyle(
                      color: UIColors.grey, fontWeight: FontWeight.normal),
                  hintText: widget.hint,
                  errorText: widget.isInputValid! ? null : widget.error,
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: UIColors.red),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(90.0),
                    ),
                    gapPadding: 4.0,
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: UIColors.red),
                    borderRadius: const BorderRadius.all(
                      Radius.circular(90.0),
                    ),
                    gapPadding: 4.0,
                  ),
                ),
              ),
              Positioned(
                right: 0,
                top: widget.isInputValid! ? 0 : -17,
                // values to put the button in a good position
                bottom: widget.isInputValid! ? 0 : 5,
                // must be another way, but I'm in a rush
                child: Material(
                  // needed
                  color: UIColors.transparent,
                  child: InkWell(
                    onTap: () => widget.onClick!(widget.myController!.text),
                    child: Image.asset(
                      "assets/images/add_button.png",
                      width: widget.isInputValid! ? 70 : 70,
                      fit: widget.isInputValid! ? BoxFit.fill : BoxFit.fitWidth,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ]));
  }

  TextInputType getKeyboardType() {
    switch (widget.type) {
      case TextFieldType.password:
        return TextInputType.text;
      case TextFieldType.email:
        return TextInputType.emailAddress;
      case TextFieldType.name:
        return TextInputType.name;
      default:
        return TextInputType.text;
    }
  }

  bool inputIsPassword() {
    if (widget.type == TextFieldType.password) {
      return true;
    } else {
      return false;
    }
  }
}
