import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class HomeMainButtons extends StatelessWidget {
  const HomeMainButtons({Key? key, this.onClick, this.isDoctorButton})
      : super(key: key);

  final Function? onClick;
  final bool? isDoctorButton;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onClick!(),
      child: containerButton(isDoctorButton!),
    );
  }
}

Widget containerButton(bool isDoctorButton) {
  return Container(
    height: 106,
    width: 166,
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage(
          isDoctorButton == true
              ? 'assets/images/specialities_background_button.png'
              : 'assets/images/specialities_background_button.png',
        ),
        fit: BoxFit.cover,
      ),
    ),
    child: Container(
      padding: getPadding(isDoctorButton),
      child: Text(
        isDoctorButton == true ? "Doctores" : "Especialidad",
        style: TextStyle(
            fontWeight: FontWeight.bold, fontSize: 12, color: UIColors.white),
        textAlign: (isDoctorButton == true ? TextAlign.start : TextAlign.end),
      ),
      alignment: (isDoctorButton == true
          ? Alignment.bottomRight
          : Alignment.bottomLeft),
    ),
  );
}

EdgeInsets getPadding(bool isDoctorButton) {
  if (isDoctorButton) {
    return EdgeInsets.only(right: 12, bottom: 9);
  } else {
    return EdgeInsets.only(left: 12, bottom: 9);
  }
}
