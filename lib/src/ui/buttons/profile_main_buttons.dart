import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class ProfileMainButtons extends StatelessWidget {
  const ProfileMainButtons({Key? key, this.onClick, this.isExpedientButton})
      : super(key: key);

  final Function? onClick;
  final bool? isExpedientButton;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onClick!(),
      child: myContainerButton(isExpedientButton),
    );
  }
}

Widget myContainerButton(bool? isExpedientButton) {
  return Container(
    height: 106,
    width: 160,
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage(
          isExpedientButton == true
              ? 'assets/images/bg_personal_data.png'
              : 'assets/images/bg_family.png',
        ),
        fit: BoxFit.cover,
      ),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(children: <Widget>[
          Container(
            padding: EdgeInsets.only(left: 12),
            child: Image.asset(
              isExpedientButton == true
                  ? 'assets/images/icon_personal_data.png'
                  : 'assets/images/icon_family.png',
              height: 25,
              width: 30,
            ),
          ),
          Container(
            width: 100,
            padding: EdgeInsets.only(left: 3),
            child: Text(
              isExpedientButton == true
                  ? 'Mi expediente médico'
                  : 'Mis dependientes',
              textAlign: TextAlign.left,
              maxLines: 2,
              style: TextStyle(
                  color: UIColors.white,
                  fontSize: 15,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ])
      ],
    ),
  );
}

EdgeInsets getButtonPadding(bool isDoctorButton) {
  if (isDoctorButton) {
    return EdgeInsets.only(right: 12, bottom: 9);
  } else {
    return EdgeInsets.only(left: 12, bottom: 9);
  }
}
