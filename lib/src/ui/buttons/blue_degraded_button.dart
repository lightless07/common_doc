import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class BlueDegradedButton extends StatefulWidget {
  const BlueDegradedButton({Key? key, this.title, this.padding, this.onClick})
      : super(key: key);

  final String? title;
  final EdgeInsets? padding;
  final Function? onClick;

  @override
  BlueDegradedButtonState createState() => BlueDegradedButtonState();
}

class BlueDegradedButtonState extends State<BlueDegradedButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60.0,
      child: MaterialButton(
        onPressed: () {
          widget.onClick!();
        },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(80.0)),
        padding: EdgeInsets.fromLTRB(widget.padding!.left,
            widget.padding!.top, widget.padding!.right, widget.padding!.bottom),
        child: Ink(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: <Color>[
                  UIColors.blueButtonStartDegradedColor,
                  UIColors.blueButtonEndDegradedColor
                ],
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
              ),
              borderRadius: BorderRadius.circular(30.0)),
          child: Container(
            alignment: Alignment.center,
            child: Text(
              widget.title!,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: UIColors.white, fontWeight: FontWeight.normal),
            ),
          ),
        ),
      ),
    );
  }
}
