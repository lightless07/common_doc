import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CustomTextButton extends StatefulWidget {
  const CustomTextButton({Key? key, this.title, this.padding, this.onClick})
      : super(key: key);

  final Function? onClick;
  final EdgeInsets? padding;
  final String? title;

  @override
  CustomTextButtonState createState() => CustomTextButtonState();
}

class CustomTextButtonState extends State<CustomTextButton> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      padding: EdgeInsets.fromLTRB(widget.padding!.left, widget.padding!.top,
          widget.padding!.right, widget.padding!.bottom),
      child: MaterialButton(
        onPressed: () {
          widget.onClick!();
        },
        child: Text(
          widget.title!,
          textAlign: TextAlign.right,
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 11,
              color: UIColors.forgotPasswordFontColor),
        ),
      ),
    );
  }
}
