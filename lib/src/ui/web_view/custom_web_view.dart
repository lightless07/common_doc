import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

final GlobalKey<WebViewContainerState> webViewKey =
    GlobalKey<WebViewContainerState>();

class CustomWebView extends StatefulWidget {
  @override
  _CustomWebViewState createState() => _CustomWebViewState();
}

class _CustomWebViewState extends State<CustomWebView> {
  @override
  Widget build(BuildContext context) {
    final String? myUrl = ModalRoute.of(context)!.settings.arguments as String?;

    return Scaffold(
        appBar: AppBar(
          title: Text("Doc"),
          leading: IconButton(
              icon: Icon(Icons.arrow_back_rounded),
              onPressed: () {
                Navigator.pop(context);
              }),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  webViewKey.currentState?.loadHome();
                }),
            IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () {
                  webViewKey.currentState?.reloadWebView();
                }),
          ],
        ),
        body: WebViewContainer(key: webViewKey, myUrl: myUrl));
  }
}

class WebViewContainer extends StatefulWidget {
  WebViewContainer({Key? key, String? url, this.myUrl}) : super(key: key);

  final String? myUrl;

  @override
  WebViewContainerState createState() => WebViewContainerState();
}

class WebViewContainerState extends State<WebViewContainer> {
  WebViewController? _webViewController;
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  num _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: _stackToView as int?,
      children: <Widget>[
        Column(
          children: <Widget>[
            Expanded(
              child: WebView(
                initialUrl: widget.myUrl == null
                    ? "https://www.facebook.com/"
                    : "https://www.google.co.in/",
                javascriptMode: JavascriptMode.unrestricted,
                onPageFinished: _handleLoad,
                onWebViewCreated: (WebViewController webViewController) {
                  _controller.complete(webViewController);
                  _webViewController = webViewController;
                },
                //TODO: revisar que funcione los webviews despues de este ajuste
                /*
                javascriptChannels: <JavascriptChannel>[
                  _toasterJavascriptChannel(context),
                ].toSet(),
                 */
                javascriptChannels: <JavascriptChannel>{
                  _toasterJavascriptChannel(context),
                },
                navigationDelegate: (NavigationRequest request) {
                  if (request.url.startsWith('https://www.youtube.com/')) {
                    print('blocking navigation to $request}');
                    return NavigationDecision.prevent;
                  }
                  print('allowing navigation to $request');
                  return NavigationDecision.navigate;
                },
                onPageStarted: (String url) {
                  print('Page started loading: $url');
                },
                gestureNavigationEnabled: true,
              ),
            )
          ],
        ),
        Container(
            child: Center(
          child: CircularProgressIndicator(),
        )),
      ],
    );
  }

  JavascriptChannel _toasterJavascriptChannel(BuildContext context) {
    return JavascriptChannel(
        name: 'Toaster',
        onMessageReceived: (JavascriptMessage message) {
          // ignore: deprecated_member_use
          Scaffold.of(context).showSnackBar(
            SnackBar(content: Text(message.message)),
          );
        });
  }

  void reloadWebView() {
    setState(() {
      _stackToView = 1;
      _webViewController?.loadUrl("http://www.facebook.com");
    });
  }

  void loadHome() {
    setState(() {
      _stackToView = 1;
      _webViewController?.loadUrl("https://www.google.com.mx");
    });
  }
}
