import 'package:common_ui/src/enums/enum_type_text_fields.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CustomEditText extends StatefulWidget {
  const CustomEditText(
      {Key? key,
      this.myController,
      this.type,
      this.title,
      this.hint,
      this.error,
      this.isInputValid,
      this.isEnabled,
      this.padding})
      : super(key: key);

  final TextEditingController? myController;
  final String? title;
  final String? hint;
  final String? error;
  final EdgeInsets? padding;
  final TextFieldType? type;
  final bool? isInputValid;
  final bool? isEnabled;

  @override
  CustomEditTextState createState() => CustomEditTextState();
}

class CustomEditTextState extends State<CustomEditText> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final FocusScopeNode node = FocusScope.of(context);
    return Container(
        alignment: Alignment.centerLeft,
        padding: EdgeInsets.fromLTRB(widget.padding!.left, widget.padding!.top,
            widget.padding!.right, widget.padding!.bottom),
        child: Column(children: <Widget>[
          Container(
            alignment: Alignment.centerLeft,
            child: Text(widget.title!,
                textAlign: TextAlign.left,
                style: TextStyle(
                    fontSize: 14.0,
                    color: UIColors.editTextFontColor,
                    fontWeight: FontWeight.normal)),
            padding: EdgeInsets.fromLTRB(
                widget.padding!.left,
                widget.padding!.top,
                widget.padding!.right,
                widget.padding!.bottom),
          ),
          SizedBox(height: 5),
          TextField(
            controller: widget.myController,
            onEditingComplete: () => node.nextFocus(),
            // Move focus to next
            enabled: widget.isEnabled,
            keyboardType: getKeyboardType(),
            obscureText: inputIsPassword(),
            style: TextStyle(
                fontSize: 14.0,
                color: UIColors.editTextFontColor,
                fontWeight: FontWeight.bold),
            decoration: InputDecoration(
              /*prefixIcon: Icon(
                getIcon(),
                  color: UIColors.editTextBorderColor
              ),*/
              border: OutlineInputBorder(
                borderRadius: const BorderRadius.all(
                  Radius.circular(90.0),
                ),
                borderSide: BorderSide(
                  color: UIColors.editTextBorderColor,
                ),
              ),
              isDense: true,
              contentPadding:
                  EdgeInsets.only(left: 32, bottom: 16, top: 16, right: 32),
              filled: true,
              hintStyle: TextStyle(
                  color: UIColors.grey, fontWeight: FontWeight.normal),
              hintText: widget.hint,
              errorText: widget.isInputValid! ? null : widget.error,
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: UIColors.red),
                borderRadius: const BorderRadius.all(
                  Radius.circular(90.0),
                ),
                gapPadding: 4.0,
              ),
              errorStyle: TextStyle(color: UIColors.red),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: UIColors.red),
                borderRadius: const BorderRadius.all(
                  Radius.circular(90.0),
                ),
                gapPadding: 4.0,
              ),
            ),
          ),
        ]));
  }

  TextInputType getKeyboardType() {
    switch (widget.type) {
      case TextFieldType.password:
        return TextInputType.text;
      case TextFieldType.email:
        return TextInputType.emailAddress;
      case TextFieldType.name:
        return TextInputType.name;
      default:
        return TextInputType.text;
    }
  }

  bool inputIsPassword() {
    if (widget.type == TextFieldType.password) {
      return true;
    } else {
      return false;
    }
  }
}
