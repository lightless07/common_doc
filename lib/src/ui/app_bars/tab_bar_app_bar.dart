import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TabBarAppBar extends StatefulWidget implements PreferredSizeWidget {
  const TabBarAppBar({Key? key, this.title, this.children, this.onClick})
      : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(130);

  final String? title;
  final List<String>? children;
  final Function? onClick;

  @override
  TabBarAppBarState createState() => TabBarAppBarState();
}

class TabBarAppBarState extends State<TabBarAppBar> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: widget.children!.length,
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 130.0,
            centerTitle: true,
            elevation: 5.0,
            backgroundColor: UIColors.white,
            // status bar color
            systemOverlayStyle: SystemUiOverlayStyle.light,
            // status bar
            title: Container(
              child: SafeArea(
                minimum: const EdgeInsets.only(left: 16, right: 16, top: 16),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Text(widget.title!,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: UIColors.mainTextFontColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 26)),
                    Container(
                      margin: EdgeInsets.only(top: 32, bottom: 20),
                      child: Center(
                        child: Container(
                          height: 30,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                            color: UIColors.tabBackgroundColor,
                            borderRadius: BorderRadius.circular(
                              25.0,
                            ),
                          ),
                          child: TabBar(
                            labelColor: UIColors.tabSelectedFontColor,
                            unselectedLabelColor:
                                UIColors.tabUnSelectedFontColor,
                            indicator: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                25.0,
                              ),
                              color: UIColors.tabBarDefaultColor,
                            ),
                            onTap: (int index) {
                              widget.onClick!(index);
                            },
                            tabs: getTabs(),
                          ),
                        ),
                      ),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(100)),
                          border: Border.all(color: UIColors.white, width: 3)),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  List<Tab> getTabs() {
    List<Tab> tabs = <Tab>[];
    for (int i = 0; i < widget.children!.length; i++) {
      tabs.add(Tab(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              widget.children![i],
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ));
    }
    return tabs;
  }
}
