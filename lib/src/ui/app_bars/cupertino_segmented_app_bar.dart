import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CupertinoSegmentedAppBar extends StatefulWidget
    implements PreferredSizeWidget {
  const CupertinoSegmentedAppBar(
      {Key? key, this.title, this.children, this.onClick})
      : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(150);

  final String? title;
  final Map<int, Widget>? children;
  final Function? onClick;

  @override
  CupertinoSegmentedAppBarState createState() =>
      CupertinoSegmentedAppBarState();
}

class CupertinoSegmentedAppBarState extends State<CupertinoSegmentedAppBar> {
  int? _currentSelection = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 150.0,
      backgroundColor: UIColors.editTextColor,
      // status bar color
      systemOverlayStyle: SystemUiOverlayStyle.light,
      // status bar
      centerTitle: true,
      toolbarTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
      title: Padding(
        padding: const EdgeInsets.only(top: 30.0, bottom: 30.0),
        child: Container(
          child: SafeArea(
            minimum: const EdgeInsets.only(left: 16, right: 16, top: 16),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(widget.title!,
                    textAlign: TextAlign.center,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
                SizedBox(
                  height: 32,
                ),
                Container(
                  child: Center(
                    child: CupertinoSlidingSegmentedControl(
                        groupValue: _currentSelection,
                        backgroundColor: UIColors.teal.withOpacity(.2),
                        thumbColor: UIColors.blue,
                        children: widget.children!,
                        onValueChanged: (int? value) {
                          setState(() {
                            _currentSelection = value;
                            widget.onClick!(value);
                          });
                        }),
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(100)),
                      border: Border.all(color: UIColors.white, width: 3)),
                ),
                SizedBox(
                  height: 32,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
