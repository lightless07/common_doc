import 'package:common_ui/src/resources/custom_icons.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class TitleSubtitleAppBar extends StatefulWidget {
  const TitleSubtitleAppBar(
      {Key? key,
      required this.title,
      this.subTitle,
      this.chooseProfile,
      this.searchTitle,
      this.back,
      this.onClick})
      : super(key: key);

  final String title;
  final String? subTitle;
  final String? searchTitle;
  final bool? chooseProfile;
  final bool? back;
  final Function? onClick;

  @override
  TitleSubtitleAppBarState createState() => TitleSubtitleAppBarState();
}

class TitleSubtitleAppBarState extends State<TitleSubtitleAppBar> {
  late double barHeight; // change this for different heights
  EdgeInsets? titlePadding;

  @override
  void initState() {
    barHeight = 60;
    titlePadding = widget.subTitle == null
        ? EdgeInsets.only(top: 0)
        : EdgeInsets.only(top: 9);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Material(
      elevation: 5.0,
      color: UIColors.white,
      child: Container(
          padding: EdgeInsets.only(top: statusBarHeight),
          height: statusBarHeight + barHeight,
          child: Column(
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.only(left: 16),
                            child: Visibility(
                              child: Container(
                                  child: loadBackButtonAppBar(context)),
                              visible: widget.back!,
                              maintainSize: true,
                              maintainAnimation: true,
                              maintainState: true,
                            )),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: titlePadding,
                          child: Text(
                            widget.title,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: UIColors.mainTextFontColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          alignment: Alignment.center,
                        ),
                        widget.subTitle == null
                            ? SizedBox()
                            : Container(
                                padding: EdgeInsets.only(top: 5, bottom: 8),
                                child: Text(
                                  widget.subTitle!,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  textDirection: TextDirection.ltr,
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: UIColors.mainTextFontColor,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 16,
                                  ),
                                ),
                                alignment: Alignment.center,
                              )
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Visibility(
                          child: Container(
                              padding: EdgeInsets.only(right: 16),
                              child: loadBackButtonAppBar(context)),
                          visible: false,
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                        )
                      ],
                    ),
                  ]),
            ],
          )),
    );
  }
}
