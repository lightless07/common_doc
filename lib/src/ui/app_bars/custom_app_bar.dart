import 'package:common_ui/card_views.dart';
import 'package:common_ui/resources.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

AppBar simpleAppBar(String title) {
  return AppBar(
    backgroundColor: Colors.white,
    toolbarHeight: 60,
    systemOverlayStyle: SystemUiOverlayStyle.light,
    // status bar
    title: Text(title),
    centerTitle: true,
    toolbarTextStyle: TextStyle(
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.bold,
    ),
  );
}

AppBar homeAppBar(Function onClick) {
  return AppBar(
    toolbarHeight: 80.0,
    elevation: 5.0,
    backgroundColor: Colors.white,
    // status bar color
    systemOverlayStyle: SystemUiOverlayStyle.light,
    // status bar
    flexibleSpace: FlexibleSpaceBar(
      centerTitle: true,
      title: Container(
        child: SafeArea(
          minimum: const EdgeInsets.only(left: 16, right: 16, top: 16),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Visibility(
                          maintainSize: true,
                          maintainAnimation: true,
                          maintainState: true,
                          visible: false,
                          child: Container(
                            alignment: Alignment.center,
                            child: SelectNewProfileTile(
                              profileUrl: "https://placeimg.com/640/480/any",
                              onClick: onClick,
                            ),
                          )),
                      //Container(width: 135.0, height: 0.0),
                      Container(
                        alignment: Alignment.center,
                        child: Image.asset(
                          'assets/images/app_bar_logo.png',
                        ),
                      ),
                      Container(
                        alignment: Alignment.center,
                        child: SelectNewProfileTile(
                          profileUrl: "https://placeimg.com/640/480/any",
                          onClick: onClick,
                        ),
                      )
                    ]),
                //SizedBox(height: 8,)
              ]),
        ),
      ),
    ),
  );
}

SliverAppBar simpleSliverAppBar(String title) {
  return SliverAppBar(
    pinned: true,
    expandedHeight: dimCoverHeight,
    flexibleSpace: FlexibleSpaceBar(
      background: Image.asset(
        'assets/graphics/login/welcome.png',
        fit: BoxFit.cover,
      ),
      centerTitle: false,
      title: Text(title),
    ),
  );
}
