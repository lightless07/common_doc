import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_segmented_control/material_segmented_control.dart';

class MaterialSegmentedAppBar extends StatefulWidget
    implements PreferredSizeWidget {
  const MaterialSegmentedAppBar(
      {Key? key, this.title, this.children, this.onClick})
      : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(150);

  final String? title;
  final Map<int, Widget>? children;
  final Function? onClick;

  @override
  MaterialSegmentedAppBarState createState() => MaterialSegmentedAppBarState();
}

class MaterialSegmentedAppBarState extends State<MaterialSegmentedAppBar> {
  int _currentSelection = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      toolbarHeight: 150.0,
      backgroundColor: UIColors.editTextColor,
      // status bar color
      systemOverlayStyle: SystemUiOverlayStyle.light,
      // status bar
      centerTitle: true,
      toolbarTextStyle: TextStyle(
        color: Colors.black,
        fontSize: 16,
        fontWeight: FontWeight.bold,
      ),
      title: Container(
        child: SafeArea(
          minimum: const EdgeInsets.only(left: 16, right: 16, top: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(widget.title!,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
              SizedBox(
                height: 32,
              ),
              Container(
                child: MaterialSegmentedControl(
                  horizontalPadding: EdgeInsets.all(1),
                  verticalOffset: 15,
                  selectionIndex: _currentSelection,
                  borderColor: UIColors.grey,
                  selectedColor: UIColors.secondTextFontColor,
                  unselectedColor: UIColors.white,
                  disabledChildren: null,
                  borderRadius: 100.0,
                  children: widget.children!,
                  onSegmentTapped: (int index) {
                    setState(() {
                      _currentSelection = index;
                      widget.onClick!(index);
                    });
                  },
                ),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(100)),
                    border: Border.all(color: UIColors.white, width: 3)),
              ),
              SizedBox(
                height: 32,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
