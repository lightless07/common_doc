import 'package:common_ui/src/resources/custom_icons.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/card_views/select_new_profile_tile.dart';
import 'package:flutter/material.dart';

class SearchChooseProfileAppBar extends StatefulWidget
    implements PreferredSizeWidget {
  const SearchChooseProfileAppBar(
      {Key? key,
      this.title,
      this.chooseProfile,
      this.searchController,
      this.searchTitle,
      this.searchBar,
      this.back,
      this.onSearch,
      this.onClick})
      : super(key: key);

  @override
  Size get preferredSize => const Size.fromHeight(100);

  final String? title;
  final String? searchTitle;
  final bool? chooseProfile;
  final TextEditingController? searchController;
  final bool? searchBar;
  final bool? back;
  final Function? onSearch;
  final Function? onClick;

  @override
  SearchChooseProfileAppBarState createState() =>
      SearchChooseProfileAppBarState();
}

class SearchChooseProfileAppBarState extends State<SearchChooseProfileAppBar> {
  late double barHeight; // change this for different heights

  @override
  void initState() {
    barHeight = widget.searchBar! ? 120.0 : 60;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Material(
      elevation: 5.0,
      color: UIColors.white,
      child: Container(
          padding: EdgeInsets.only(top: statusBarHeight),
          height: statusBarHeight + barHeight,
          child: Column(
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Visibility(
                        maintainSize: true,
                        maintainAnimation: true,
                        maintainState: true,
                        visible: widget.back!,
                        child: Container(
                          padding: EdgeInsets.only(left: 16),
                          child: loadBackButtonAppBar(context),
                          width: 100,
                          alignment: Alignment.centerLeft,
                        )),
                    Container(
                      alignment: Alignment.center,
                      child: Image.asset(
                        'assets/images/app_bar_logo.png',
                      ),
                    ),
                    Visibility(
                      maintainSize: true,
                      maintainAnimation: true,
                      maintainState: true,
                      visible: widget.chooseProfile!,
                      child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(right: 16),
                          child: SelectNewProfileTile(
                            profileUrl: "https://placeimg.com/640/480/any",
                            onClick: widget.onClick,
                          )),
                    )
                  ]),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <
                  Widget>[
                widget.searchBar!
                    ? Flexible(
                        child: Container(
                            padding: EdgeInsets.only(
                                left: 32, right: 32, top: 10, bottom: 10),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                    child: Container(
                                        height: 40,
                                        alignment: Alignment.center,
                                        child: TextField(
                                          controller: widget.searchController,
                                          onSubmitted: widget.onSearch!(
                                              widget.searchController!.text),
                                          textInputAction:
                                              TextInputAction.search,
                                          keyboardType: TextInputType.text,
                                          obscureText: false,
                                          textAlign: TextAlign.left,
                                          decoration: InputDecoration(
                                            prefixIcon: Icon(
                                              Icons.search,
                                              color: UIColors.black,
                                              size: 20,
                                            ),
                                            contentPadding: EdgeInsets.only(
                                              bottom:
                                                  20, // HERE THE IMPORTANT PART
                                            ),
                                            border: OutlineInputBorder(
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(90.0),
                                              ),
                                              borderSide: BorderSide(
                                                color: UIColors
                                                    .editTextBorderColor,
                                              ),
                                            ),
                                            filled: true,
                                            hintStyle: TextStyle(
                                                color: UIColors.grey,
                                                fontSize: 14.0),
                                            hintText: widget.searchTitle,
                                            fillColor: UIColors.editTextColor,
                                            //labelText: widget.title,
                                            //errorText: widget.isInputValid ? null : widget.error,
                                            errorBorder: OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: UIColors.red),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(90.0),
                                              ),
                                              gapPadding: 4.0,
                                            ),
                                            focusedErrorBorder:
                                                OutlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: UIColors.red),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(90.0),
                                              ),
                                              gapPadding: 4.0,
                                            ),
                                          ),
                                        )))
                              ],
                            )))
                    : Container(
                        height: 0.0,
                      ),
                widget.searchController!.text.isNotEmpty
                    ? IconButton(
                        icon: Icon(
                          Icons.clear,
                          color: UIColors.black,
                        ),
                        onPressed: () {
                          setState(() {
                            widget.searchController!.clear();
                          });
                        })
                    : Container(
                        height: 0.0,
                      )
              ]),
            ],
          )),
    );
  }
}
