import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';
import 'package:material_segmented_control/material_segmented_control.dart';

class CustomSegmented extends StatefulWidget {
  const CustomSegmented({Key? key, this.children, this.onClick})
      : super(key: key);

  final Map<int, Widget>? children;
  final Function? onClick;

  @override
  CustomSegmentedState createState() => CustomSegmentedState();
}

class CustomSegmentedState extends State<CustomSegmented> {
  int _currentSelection = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      child: MaterialSegmentedControl(
        horizontalPadding: EdgeInsets.all(1),
        verticalOffset: 15,
        selectionIndex: _currentSelection,
        borderColor: UIColors.grey,
        selectedColor: UIColors.secondTextFontColor,
        unselectedColor: UIColors.white,
        disabledChildren: null,
        borderRadius: 100.0,
        children: widget.children!,
        onSegmentTapped: (int index) {
          setState(() {
            _currentSelection = index;
            widget.onClick!(index);
          });
        },
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(100)),
          border: Border.all(color: UIColors.white, width: 3)),
    );
  }
}
