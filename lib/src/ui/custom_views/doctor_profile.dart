import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class DoctorProfile {
  static Widget showProfile(
      String avatar, String doctorName, String doctorBio, String doctorIDCard) {
    return Padding(
        padding: EdgeInsets.only(top: 16, bottom: 16, left: 32, right: 32),
        child: Row(children: <Widget>[
          doctorAvatarImage(avatar),
          Expanded(
              child: Padding(
                  padding: EdgeInsets.only(left: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(bottom: 10),
                          child: Text(
                            doctorName,
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 22.0,
                                fontWeight: FontWeight.bold,
                                color: UIColors.black),
                          )),
                      Container(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Text(
                            doctorBio,
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.normal,
                                color: UIColors.black),
                          )),
                      Container(
                          padding: EdgeInsets.only(bottom: 8),
                          child: Text(
                            doctorIDCard,
                            maxLines: 2,
                            overflow: TextOverflow.visible,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 10.0,
                                fontWeight: FontWeight.bold,
                                color: UIColors.black),
                          )),
                    ],
                  )))
        ]));
  }

  static Widget doctorAvatarImage(String avatar) {
    return Container(
      padding: EdgeInsets.all(2.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(90.0)),
          color: UIColors.blue),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(90.0), //or 15.0
        child: Container(
          height: 100.0,
          width: 100.0,
          color: Color(0xffffffff),
          child: Image.network(
            avatar,
            height: 100.0,
            width: 100.0,
          ),
        ),
      ),
    );
  }

  static Widget userAvatarImage(String avatar) {
    return Container(
      //padding: new EdgeInsets.all(2.0),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(90.0)),
          color: UIColors.white),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(90.0), //or 15.0
        child: Container(
          height: 120.0,
          width: 120.0,
          color: Color(0xffffffff),
          child: Image.network(
            avatar,
            height: 100.0,
            width: 100.0,
          ),
        ),
      ),
    );
  }
}
