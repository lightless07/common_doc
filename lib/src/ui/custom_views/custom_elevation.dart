import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CustomElevation extends StatelessWidget {
  CustomElevation({required this.child});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Material(
        borderRadius: BorderRadius.circular(30.0),
        elevation: 220.0,
        child: Container(
          decoration: BoxDecoration(
            color: UIColors.black,
            borderRadius: BorderRadius.circular(30.0),
            // the box shawdow property allows for fine tuning as aposed to shadowColor
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: UIColors.black,
                  // offset, the X,Y coordinates to offset the shadow
                  offset: Offset(
                    15.0, // Move to right 10  horizontally
                    15.0,
                  ),
                  // blurRadius, the higher the number the more smeared look
                  blurRadius: 80.0,
                  spreadRadius: 4.0)
            ],
          ),
          child: child,
        ));
  }
}
