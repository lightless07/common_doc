import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class ShapeLayer extends StatelessWidget {
  ShapeLayer({Key? key, this.frontLayer}) : super(key: key);

  final Widget? frontLayer;
  final Widget backLayer = Container(
    color: UIColors.black,
  );

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        backLayer,
        Material(
          elevation: 16.0,
          shape: BeveledRectangleBorder(
            borderRadius: BorderRadius.only(topLeft: Radius.circular(46.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Expanded(child: frontLayer!),
            ],
          ),
        ),
      ],
    );
  }
}
