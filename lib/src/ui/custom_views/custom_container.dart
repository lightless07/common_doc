import 'package:common_ui/src/models/core/treatment_indication.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/text_box/description_text_widget.dart';
import 'package:flutter/material.dart';

import '../buttons/blue_button.dart';

Widget showSpecialitiesContainer(List<String> specialities) {
  return Container(
    padding: EdgeInsets.only(left: 32, right: 32, top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        titleWithUnderline("Especialidades"),
        Wrap(
          spacing: 6.0,
          runSpacing: 6.0,
          children: List<Widget>.generate(specialities.length, (int index) {
            return Container(
              margin: EdgeInsets.only(right: 5.0),
              padding: const EdgeInsets.all(5.0),
              decoration: BoxDecoration(
                border:
                    Border.all(width: 1.0, color: UIColors.blueDividerColor),
                borderRadius: BorderRadius.all(
                    Radius.circular(95.0) //         <--- border radius here
                    ),
              ),
              child: Text(
                specialities[index],
                style: TextStyle(
                    fontSize: 9.0,
                    color: UIColors.secondTextFontColor,
                    fontWeight: FontWeight.bold),
              ),
            );
          }),
        ),
        customDivider(),
      ],
    ),
  );
}

Widget showDoctorBioContainer(String description) {
  return Container(
      padding: EdgeInsets.only(left: 32, right: 32),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        titleWithUnderline("Bio"),
        DescriptionTextWidget(text: description),
        customDivider(),
      ]));
}

Widget showMeetRecommendationsContainer(String description) {
  return Container(
      padding: EdgeInsets.only(left: 32, right: 32),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        titleWithUnderline("Recomendaciones"),
        Container(
          margin: EdgeInsets.only(top: 32),
          alignment: Alignment.centerLeft,
          child: Text(
            description,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: UIColors.mainTextFontColor),
          ),
        ),
        customDivider(),
      ]));
}

Widget showTreatmentPrescriptionsContainer(String description) {
  return Container(
      padding: EdgeInsets.only(left: 32, right: 32),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        titleWithUnderline("Prescripción"),
        Container(
          margin: EdgeInsets.only(top: 10),
          alignment: Alignment.centerLeft,
          child: Text(
            description,
            style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.normal,
                color: UIColors.mainTextFontColor),
          ),
        ),
        customDivider(),
      ]));
}

Widget showTreatmentIndicationsContainer(
    List<TreatmentIndication> treatmentIndicationsList) {
  return Container(
      padding: EdgeInsets.only(left: 32, right: 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: fillTreatmentIndications(treatmentIndicationsList),
      ));
}

List<Widget> fillTreatmentIndications(
    List<TreatmentIndication> treatmentIndicationsList) {
  List<Widget> list = [];
  list.add(Padding(
    padding: EdgeInsets.only(bottom: 10),
    child: titleWithUnderline("Indicaciones"),
  ));
  for (int i = 0; i < treatmentIndicationsList.length; i++) {
    list.add(Container(
        width: double.infinity,
        padding: const EdgeInsets.all(20.0),
        margin: const EdgeInsets.only(bottom: 20),
        decoration: BoxDecoration(
          border: Border.all(color: UIColors.blueDividerColor),
          color: UIColors.white,
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(bottom: 25),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: Image.asset(
                      'assets/images/medicine_pill_icon.png',
                      height: 19,
                      width: 19,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      treatmentIndicationsList[i].name,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: UIColors.mainTextFontColor),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: Text(
                      "Recurrencia",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: UIColors.mainTextFontColor),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      treatmentIndicationsList[i].frequencyDetail,
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: UIColors.secondTextFontColor),
                    ),
                  ),
                ],
              ),
            ),
            customDivider(),
            Padding(
              padding: EdgeInsets.only(top: 5),
              child: Row(
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: Text(
                      "Tomas",
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: UIColors.mainTextFontColor),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      alignment: Alignment.centerRight,
                      child: showTreatmentDays(
                          treatmentIndicationsList[i].totalShots,
                          treatmentIndicationsList[i].shotsTaken),
                    ),
                  ),
                ],
              ),
            ),
          ],
        )));
  }
  return list;
}

Widget showTreatmentDays(int totalTreatmentDays, int takenTreatmentDays) {
  List<Widget> list = [];
  for (int i = 0; i < totalTreatmentDays; i++) {
    Color currentColor;
    if (i < takenTreatmentDays) {
      currentColor = UIColors.grey;
    } else {
      currentColor = UIColors.blue;
    }
    list.add(Container(
      margin: EdgeInsets.only(right: 10, bottom: 10),
      width: 10,
      height: 10,
      decoration: BoxDecoration(shape: BoxShape.circle, color: currentColor),
    ));
  }
  Widget container = Align(
      alignment: Alignment.centerRight,
      child: Wrap(
        children: list,
      ));
  return container;
}

Widget showDoctorNameAndSpeciality(String name, String speciality) {
  return Container(
      width: double.infinity,
      padding: const EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        border: Border.all(color: UIColors.blueDividerColor),
        color: UIColors.white,
        borderRadius: BorderRadius.all(Radius.circular(5.0)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            name,
            style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: UIColors.mainTextFontColor),
          ),
          Text(
            speciality,
            style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.normal,
                color: UIColors.secondTextFontColor),
          ),
        ],
      ));
}

Container titleWithUnderline(String title) {
  return Container(
    margin: EdgeInsets.only(left: 0, top: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: UIColors.black),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
          child: Container(
            alignment: Alignment.centerLeft,
            height: 1.0,
            width: 20,
            color: UIColors.grayDividerColor,
          ),
        ),
      ],
    ),
  );
}

Padding customDivider() {
  return Padding(
      padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
      child: Divider(
        height: 1,
        color: UIColors.lightGrayDividerColor,
      ));
}

Widget meetRow(String title, String detail, bool isButton, Function onClick) {
  return Container(
      padding: EdgeInsets.only(left: 32, right: 32),
      child: Column(children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              title,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.normal,
                  color: UIColors.mainTextFontColor),
            ),
          ),
          Container(
            alignment: Alignment.centerRight,
            child: Text(
              detail,
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.normal,
                  color: UIColors.secondTextFontColor),
            ),
          ),
        ]),
        isButton
            ? Row(mainAxisAlignment: MainAxisAlignment.end, children: [
                Container(
                  padding: EdgeInsets.only(top: 15, bottom: 5),
                  width: 104,
                  alignment: Alignment.centerRight,
                  child: BlueButton(
                    isDisabled: false,
                    title: "Ver ubicación",
                    onClick: () {
                      onClick();
                    },
                  ),
                ),
              ])
            : SizedBox(
                height: 5,
              ),
        customDivider(),
      ]));
}

Widget profileRow(
    String title, String detail, bool isButton, Function onClick) {
  return GestureDetector(
    onTap: () => onClick(),
    child: Container(
        padding: EdgeInsets.only(left: 32, right: 32),
        child: Column(children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
            //Expanded(
            //child:
            Container(
              alignment: Alignment.centerLeft,
              child: Text(
                title,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.normal,
                    color: UIColors.mainTextFontColor),
              ),
            ),
            //flex: 1,
            //),
            Expanded(
              child: Container(
                alignment: Alignment.centerRight,
                padding: EdgeInsets.only(left: 30),
                child: Text(
                  detail,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      color: UIColors.secondTextFontColor),
                ),
              ),
              flex: 3,
            ),
            Icon(
              Icons.keyboard_arrow_right,
              color: UIColors.secondTextFontColor,
              size: 20,
            ),
          ]),
          customDivider(),
        ])),
  );
}

Widget overlappedWidgets(Widget firstWidget, Widget secondWidget) {
  return Container(
    // alignment: FractionalOffset.center,
    child: Stack(
      //alignment:new Alignment(x, y)
      children: <Widget>[
        firstWidget,
        Positioned(
          right: 0.0,
          top: 40,
          child: secondWidget,
        ),
      ],
    ),
  );
}
