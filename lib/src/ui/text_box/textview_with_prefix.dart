import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class TextViewWithPrefix extends StatefulWidget {
  TextViewWithPrefix({
    Key? key,
    required this.amount,
    required this.isEnabled,
    required this.padding,
  }) : super(key: key);

  final String amount;
  final EdgeInsets padding;
  final bool isEnabled;

  @override
  TextViewWithPrefixState createState() => TextViewWithPrefixState();
}

class TextViewWithPrefixState extends State<TextViewWithPrefix> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding,
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(
                color: UIColors.blue,
                width: 2.0,
              ),
              borderRadius: BorderRadius.all(Radius.circular(90.0)),
            ),
            child: Icon(
              Icons.attach_money_outlined,
              color: UIColors.roundedButtonFontColor,
              size: 19,
            ),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 10),
                child: IgnorePointer(
                  ignoring: !widget.isEnabled,
                  child: Container(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(90.0)),
                        border: Border.all(
                          color: UIColors.customDropDownBorderColor,
                          width: 1,
                        ),
                        color: UIColors.customDropDownBackgroundColor),
                    child: Container(
                        padding: EdgeInsets.only(left: 15),
                        height: 50,
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            widget.amount,
                            maxLines: 2,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.normal,
                                color: UIColors.mainTextFontColor),
                          ),
                        )),
                  ),
                )),
          )
        ],
      ),
    );
  }
}
