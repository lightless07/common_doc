import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class DescriptionTextWidget extends StatefulWidget {
  DescriptionTextWidget({required this.text});

  final String text;

  @override
  _DescriptionTextWidgetState createState() => _DescriptionTextWidgetState();
}

class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
  late String firstHalf;
  late String secondHalf;

  bool flag = true;

  @override
  void initState() {
    super.initState();

    if (widget.text.length > 100) {
      firstHalf = widget.text.substring(0, 100);
      secondHalf = widget.text.substring(100, widget.text.length);
    } else {
      firstHalf = widget.text;
      secondHalf = "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: secondHalf.isEmpty
          ? Text(firstHalf)
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf),
                    style: TextStyle(
                        color: UIColors.mainTextFontColor,
                        fontWeight: FontWeight.normal,
                        fontSize: 14)),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: InkWell(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(90.0)),
                              color: UIColors.tabBarDefaultColor),
                          child: Row(children: <Widget>[
                            Icon(
                              flag ? Icons.add : Icons.remove_sharp,
                              color: UIColors.white,
                              size: 14,
                            ),
                            Text(
                              flag ? "Ver mas" : "Ver menos",
                              style: TextStyle(color: UIColors.white),
                            ),
                          ]),
                        )
                      ],
                    ),
                    onTap: () {
                      setState(() {
                        flag = !flag;
                      });
                    },
                  ),
                )
              ],
            ),
    );
  }
}
