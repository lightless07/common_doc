import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CustomTextBox extends StatefulWidget {
  const CustomTextBox({Key? key, this.title, this.padding}) : super(key: key);

  final EdgeInsets? padding;
  final String? title;

  @override
  CustomTextBoxState createState() => CustomTextBoxState();
}

class CustomTextBoxState extends State<CustomTextBox> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.fromLTRB(widget.padding!.left, widget.padding!.top,
          widget.padding!.right, widget.padding!.bottom),
      child: Text(
        widget.title!,
        textAlign: TextAlign.left,
        style: TextStyle(
            fontSize: 16, fontWeight: FontWeight.normal, color: UIColors.black),
      ),
    );
  }
}
