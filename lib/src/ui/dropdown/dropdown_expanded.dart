import 'package:common_ui/src/resources/strings.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class DropdownExpanded extends StatefulWidget {
  DropdownExpanded({
    Key? key,
    required this.dropdownMenuItemList,
    required this.onChanged,
    required this.value,
  }) : super(key: key);

  final List<String> dropdownMenuItemList;
  final Function onChanged;
  final String value;

  @override
  DropdownExpandedState createState() => DropdownExpandedState();
}

class DropdownExpandedState extends State<DropdownExpanded> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: ButtonTheme(
        height: 200,
        alignedDropdown: true,
        child: DropdownButton<String>(
          isExpanded: true,
          value: widget.value,
          iconSize: 30,
          icon: (null),
          style: TextStyle(
            color: UIColors.black54,
            fontSize: 16,
          ),
          hint: Text(
            Strings.hintDropDown,
            maxLines: 2,
            textDirection: TextDirection.ltr,
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.normal,
                color: UIColors.mainTextFontColor),
          ),
          onChanged: (String? value) {
            widget.onChanged(value);
          },
          items: (widget.dropdownMenuItemList.map((String item) {
                return DropdownMenuItem(
                  child: Text(
                    item,
                    maxLines: 2,
                    textDirection: TextDirection.ltr,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.normal,
                        color: UIColors.mainTextFontColor),
                  ),
                  value: item.toString(),
                );
              }).toList()),
        ),
      ),
    );
  }
}
