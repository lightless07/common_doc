import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/dropdown/dropdown_expanded.dart';
import 'package:flutter/material.dart';

class DropdownWithIcon extends StatefulWidget {
  DropdownWithIcon({
    Key? key,
    required this.dropdownMenuItemList,
    required this.onChanged,
    required this.value,
    required this.padding,
    this.isEnabled = true,
  }) : super(key: key);

  final List<String> dropdownMenuItemList;
  final Function onChanged;
  final String value;
  final bool isEnabled;
  final EdgeInsets padding;

  @override
  DropdownWithIconState createState() => DropdownWithIconState();
}

class DropdownWithIconState extends State<DropdownWithIcon> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: widget.padding,
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              border: Border.all(
                color: UIColors.blue, //                   <--- border color
                width: 2.0,
              ),
              borderRadius: BorderRadius.all(Radius.circular(
                      90.0) //                 <--- border radius here
                  ),
            ),
            child: Icon(
              Icons.access_time_outlined,
              color: UIColors.roundedButtonFontColor,
              size: 19,
            ),
          ),
          Expanded(
            child: Padding(
                padding: EdgeInsets.only(left: 10),
                child: IgnorePointer(
                  ignoring: !widget.isEnabled,
                  child: Container(
                    padding: const EdgeInsets.only(left: 10.0, right: 10.0),
                    decoration: BoxDecoration(
                        borderRadius:
                            const BorderRadius.all(Radius.circular(90.0)),
                        border: Border.all(
                          color: UIColors.customDropDownBorderColor,
                          width: 1,
                        ),
                        color: widget.isEnabled
                            ? UIColors.customDropDownBackgroundColor
                            : UIColors.grey.withAlpha(100)),
                    child: DropdownExpanded(
                      dropdownMenuItemList: widget.dropdownMenuItemList,
                      value: widget.value,
                      onChanged: (String value) {
                        widget.onChanged(value);
                      },
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }
}
