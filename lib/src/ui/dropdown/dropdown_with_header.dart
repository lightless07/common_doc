import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/dropdown/dropdown_expanded.dart';
import 'package:flutter/material.dart';

class DropdownWithHeader extends StatefulWidget {
  DropdownWithHeader({
    Key? key,
    required this.dropdownMenuItemList,
    required this.onChanged,
    required this.value,
    required this.headerTitle,
    required this.isError,
    required this.errorText,
    this.isEnabled = true,
  }) : super(key: key);

  final List<String> dropdownMenuItemList;
  final Function onChanged;
  final String value;
  final bool isEnabled;
  final bool isError;
  final String headerTitle;
  final String errorText;

  @override
  DropdownWithHeaderState createState() => DropdownWithHeaderState();
}

class DropdownWithHeaderState extends State<DropdownWithHeader> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <
        Widget>[
      Expanded(
          child: Container(
              padding: EdgeInsets.only(left: 32, right: 32),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                            padding:
                                EdgeInsets.only(left: 36, bottom: 5, right: 36),
                            child: IgnorePointer(
                              ignoring: !widget.isEnabled,
                              child: Text(
                                widget.headerTitle,
                                style: TextStyle(
                                    fontSize: 14.0,
                                    color: UIColors.mainTextFontColor,
                                    fontWeight: FontWeight.normal),
                              ),
                            )),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                            padding: EdgeInsets.only(left: 0),
                            child: IgnorePointer(
                              ignoring: !widget.isEnabled,
                              child: Container(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                decoration: BoxDecoration(
                                    borderRadius: const BorderRadius.all(
                                        Radius.circular(90.0)),
                                    border: Border.all(
                                      color: widget.isError
                                          ? UIColors.red
                                          : UIColors.black,
                                      width: 1,
                                    ),
                                    color: widget.isEnabled
                                        ? UIColors.customDropDownBackgroundColor
                                        : UIColors.grey.withAlpha(100)),
                                child: DropdownExpanded(
                                  dropdownMenuItemList:
                                      widget.dropdownMenuItemList,
                                  value: widget.value,
                                  onChanged: (String value) {
                                    widget.onChanged(value);
                                  },
                                ),
                              ),
                            )),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.only(left: 36, top: 5, right: 36),
                          child: widget.isError
                              ? Text(
                                  widget.errorText,
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      color: UIColors.red,
                                      fontWeight: FontWeight.normal),
                                )
                              : SizedBox(),
                        ),
                      )
                    ],
                  ),
                ],
              )))
    ]);
  }
}
