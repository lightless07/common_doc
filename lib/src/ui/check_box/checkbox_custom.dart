import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CheckboxCustom extends StatefulWidget {
  const CheckboxCustom({Key? key, this.isChecked}) : super(key: key);

  final bool? isChecked;

  @override
  CheckboxCustomState createState() => CheckboxCustomState();
}

class CheckboxCustomState extends State<CheckboxCustom> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: widget.isChecked!
              ? UIColors.blueDividerColor
              : UIColors.checkBoxDisabledColor),
      child: widget.isChecked!
          ? Icon(
              Icons.check,
              size: 15.0,
              color: Colors.white,
            )
          : SizedBox(
              height: 15,
              width: 15,
            ),
    );
  }
}
