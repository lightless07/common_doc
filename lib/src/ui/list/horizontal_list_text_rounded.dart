import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class HorizontalListTextRounded extends StatefulWidget {
  const HorizontalListTextRounded({Key? key, this.listItems, this.padding})
      : super(key: key);

  final List<String>? listItems;
  final EdgeInsets? padding;

  @override
  HorizontalListTextRoundedState createState() =>
      HorizontalListTextRoundedState();
}

class HorizontalListTextRoundedState extends State<HorizontalListTextRounded> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = <Widget>[];

    for (int i = 0; i < widget.listItems!.length; i++) {
      children.add(Container(
        margin: EdgeInsets.only(right: 5.0),
        padding: const EdgeInsets.only(left: 5.0, right: 5.0),
        height: 20,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          border: Border.all(width: 1.0, color: UIColors.blueDividerColor),
          borderRadius: BorderRadius.all(Radius.circular(95.0)),
        ),
        child: Text(
          widget.listItems![i],
          style: TextStyle(
              fontSize: 9.0,
              color: UIColors.secondTextFontColor,
              fontWeight: FontWeight.bold),
        ),
      ));
    }

    return Container(
        height: 20.0,
        child: ListView(scrollDirection: Axis.horizontal, children: children));
  }
}
