import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class MultiLineListRounded extends StatefulWidget {
  MultiLineListRounded({Key? key, this.listItems, this.heightItems})
      : super(key: key);

  final List<String>? listItems;
  final double? heightItems;

  @override
  MultiLineListRoundedState createState() => MultiLineListRoundedState();
}

class MultiLineListRoundedState extends State<MultiLineListRounded> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      spacing: 6.0,
      runSpacing: 6.0,
      children: List<Widget>.generate(widget.listItems!.length, (int index) {
        return Container(
          height: widget.heightItems,
          margin: EdgeInsets.only(right: 5.0),
          padding: const EdgeInsets.all(5.0),
          decoration: BoxDecoration(
            border: Border.all(width: 1.0, color: UIColors.blueDividerColor),
            borderRadius: BorderRadius.all(Radius.circular(95.0)),
          ),
          child: Text(
            widget.listItems![index],
            style: TextStyle(
                fontSize: 14.0,
                color: UIColors.secondTextFontColor,
                fontWeight: FontWeight.bold),
          ),
        );
      }),
    );
  }
}
