import 'package:common_ui/src/models/core/consulting_room_detail_model.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class HorizontalListDateRounded extends StatefulWidget {
  const HorizontalListDateRounded({
    Key? key,
    required this.calendarListItems,
    required this.isCapacityIndicator,
    required this.isSelectable,
    required this.currentDayIndex,
    required this.isScrollableToDay,
    required this.onClick,
  }) : super(key: key);

  final List<Calendar> calendarListItems;
  final bool isCapacityIndicator;
  final bool isSelectable;
  final bool isScrollableToDay;
  final int currentDayIndex;
  final Function onClick;

  @override
  HorizontalListDateRoundedState createState() =>
      HorizontalListDateRoundedState();
}

class HorizontalListDateRoundedState extends State<HorizontalListDateRounded> {
  late int selectedIndex;
  final ItemScrollController itemScrollController = ItemScrollController();
  final ItemPositionsListener itemPositionsListener =
      ItemPositionsListener.create();

  @override
  void initState() {
    selectedIndex = 0;
    WidgetsBinding.instance!.addPostFrameCallback((_) => goToRowIndex(context));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> children = <Widget>[];
    for (int i = 0; i < widget.calendarListItems.length; i++) {
      children.add(Container(
        height: 80,
        width: 50,
        margin: EdgeInsets.only(right: 10),
        child: getWidgetDate(widget.calendarListItems[i]),
      ));
    }

    return Container(
        child: ScrollablePositionedList.builder(
      itemScrollController: itemScrollController,
      itemPositionsListener: itemPositionsListener,
      scrollDirection: Axis.horizontal,
      itemCount: widget.calendarListItems.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          child: Container(
            child: children[index],
          ),
          onTap: () {
            if (widget.calendarListItems[index].able! && widget.isSelectable) {
              setState(() {
                selectedIndex = index;
                widget.onClick(index);
              });
            }
          },
        );
      },
    ));
  }

  Widget getWidgetDate(Calendar calendar) {
    if (calendar.able!) {
      if (calendar == widget.calendarListItems[selectedIndex]) {
        return Stack(
          children: <Widget>[
            getMainContainerDate(
                calendar,
                UIColors.calendarSelectedTileBgColor,
                UIColors.calendarSelectedTileBorderColor,
                UIColors.calendarSelectedTileFontColor),
            widget.isCapacityIndicator
                ? getDateCapacityIndicator(calendar)
                : SizedBox()
          ],
        );
      } else {
        return Stack(
          children: <Widget>[
            getMainContainerDate(
                calendar,
                UIColors.calendarAbleTileBgColor,
                UIColors.calendarAbleTileBorderColor,
                UIColors.calendarAbleTileFontColor),
            widget.isCapacityIndicator
                ? getDateCapacityIndicator(calendar)
                : SizedBox()
          ],
        );
      }
    } else {
      return getMainContainerDate(
          calendar,
          UIColors.calendarUnableTileBgColor,
          UIColors.calendarUnableTileBorderColor,
          UIColors.calendarUnableTileFontColor);
    }
  }

  Color getDateColorIndicator(Calendar calendar) {
    if (calendar.capacity == 1) {
      return Colors.green;
    } else {
      if (calendar.capacity == 2) {
        return UIColors.yellow;
      } else {
        return UIColors.red;
      }
    }
  }

  Widget getMainContainerDate(Calendar calendar, Color backgroundColor,
      Color borderColor, Color textColor) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(width: 1.0, color: borderColor),
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: backgroundColor),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Text(
                calendar.weekday!,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textDirection: TextDirection.ltr,
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: textColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
              alignment: Alignment.center,
            ),
            SizedBox(height: 5),
            Container(
              child: Text(
                calendar.day.toString(),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textDirection: TextDirection.ltr,
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: textColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 26,
                ),
              ),
              alignment: Alignment.center,
            ),
            SizedBox(height: 5),
            Container(
              child: Text(
                calendar.month!,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                textDirection: TextDirection.ltr,
                textAlign: TextAlign.start,
                style: TextStyle(
                  color: textColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 10,
                ),
              ),
              alignment: Alignment.center,
            ),
          ]),
    );
  }

  Positioned getDateCapacityIndicator(Calendar calendar) {
    return Positioned(
      right: 0.0,
      bottom: 0.0,
      child: Align(
          alignment: Alignment.bottomRight,
          child: Container(
            width: 10,
            height: 10,
            decoration: BoxDecoration(
                shape: BoxShape.circle, color: getDateColorIndicator(calendar)),
          )),
    );
  }

  goToRowIndex(BuildContext context) {
    if (widget.isScrollableToDay) {
      itemScrollController.jumpTo(index: widget.currentDayIndex, alignment: .4);
    }
    setState(() {
      selectedIndex = widget.currentDayIndex;
    });
  }
}
