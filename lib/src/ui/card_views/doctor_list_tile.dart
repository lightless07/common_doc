import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/list/horizontal_list_text_rounded.dart';
import 'package:flutter/material.dart';

class DoctorListTile extends StatelessWidget {
  const DoctorListTile({Key? key, this.index, this.doctorName, this.onClick})
      : super(key: key);
  final int? index;
  final String? doctorName;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onClick!(index),
      child: Container(
        margin: EdgeInsets.only(left: 32, right: 32, bottom: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
        ),
        height: 100,
        child: Card(
          elevation: 5.0,
          child: Row(
            children: <Widget>[
              Container(
                  child: Image.asset(
                'assets/graphics/icons/batman.png',
                fit: BoxFit.cover,
                height: 100,
                width: 100,
              )),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "doctorName",
                          maxLines: 2,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: UIColors.black),
                        ),
                      ),
                      getColumnChildrenHorizontalList(),
                      //getColumnChildren(),
                      Row(children: <Widget>[
                        Icon(
                          Icons.location_on_outlined,
                          size: 12,
                          color: UIColors.mainTextFontColor,
                        ),
                        Text("Mty | 3.6km")
                      ])
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget getColumnChildren() {
    List<String> specialities = <String>[
      "Maternidad",
      "Ginegologia",
      "Ginegologia",
    ];
    List<Widget> children = <Widget>[];

    for (int i = 0; i < specialities.length; i++) {
      children.add(Container(
        margin: EdgeInsets.only(right: 5.0),
        padding: const EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          border: Border.all(width: 1.0, color: UIColors.blue),
          borderRadius: BorderRadius.all(
              Radius.circular(95.0) //         <--- border radius here
              ),
        ),
        child: Text(
          specialities[i],
          style: TextStyle(fontSize: 10.0, color: UIColors.blue),
        ),
      ));
    }

    return Wrap(spacing: 6.0, runSpacing: 6.0, children: children);
  }

  Widget getColumnChildrenHorizontalList() {
    List<String> specialities = <String>[
      "Maternidad",
      "Ginegologia",
      "Ginegologia",
      "Ginegologia",
      "Ginegologia",
    ];
    return HorizontalListTextRounded(
      listItems: specialities,
    );
  }
}
