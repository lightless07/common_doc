import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/custom_views/custom_container.dart';
import 'package:flutter/material.dart';

class ProfileDataRowTile extends StatefulWidget {
  const ProfileDataRowTile({Key? key, this.title, this.detail, this.onClick})
      : super(key: key);

  final Function? onClick;
  final String? title;
  final String? detail;

  @override
  ProfileDataRowTileState createState() => ProfileDataRowTileState();
}

class ProfileDataRowTileState extends State<ProfileDataRowTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.onClick!(),
      child: containerButton(),
    );
  }

  Widget containerButton() {
    return Container(
        padding: EdgeInsets.only(left: 32, right: 32),
        child: Column(children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.title!,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.normal,
                            color: UIColors.mainTextFontColor),
                      ),
                    )),
                Expanded(
                  child: Container(
                    alignment: Alignment.centerRight,
                    padding: EdgeInsets.only(left: 30),
                    child: Text(
                      widget.detail!,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.normal,
                          color: UIColors.secondTextFontColor),
                    ),
                  ),
                  flex: 3,
                ),
                Icon(
                  Icons.keyboard_arrow_right,
                  color: UIColors.secondTextFontColor,
                  size: 20,
                ),
              ]),
          customDivider(),
        ]));
  }
}
