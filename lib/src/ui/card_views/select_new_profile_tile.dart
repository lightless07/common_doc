import 'package:common_ui/src/resources/strings.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class SelectNewProfileTile extends StatelessWidget {
  const SelectNewProfileTile({Key? key, this.profileUrl, this.onClick})
      : super(key: key);
  final String? profileUrl;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(100.0),
      ),
      elevation: 10.0,
      margin: EdgeInsets.all(10),
      child: InkWell(
          splashColor: UIColors.blue,
          onTap: () {
            onClick!();
          },
          child: Container(
            padding: EdgeInsets.only(
              left: 10,
            ),
            child: Column(children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    width: 40,
                    height: 22,
                    alignment: Alignment.center,
                    child: Text(
                      Strings.selectNewProfileTitle,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 10,
                      ),
                    ),
                  ),
                  Container(
                    width: 36,
                    height: 36,
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.all(const Radius.circular(100.0)),
                      image: DecorationImage(
                          image: NetworkImage(profileUrl!), fit: BoxFit.cover),
                    ),
                  ),
                ],
              )
            ]),
          )),
    );
  }
}
