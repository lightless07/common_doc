import 'package:common_ui/src/models/core/user_info.dart';
import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class BlogContentTile extends StatelessWidget {
  const BlogContentTile({Key? key, this.index, this.blogNew, this.onClick})
      : super(key: key);
  final int? index;
  final UserInfo? blogNew;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onClick!(index),
      child: Container(
        margin: EdgeInsets.only(left: 32, right: 32, bottom: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50.0),
        ),
        height: 100,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          elevation: 5.0,
          child: Row(
            children: <Widget>[
              Container(
                  child: Image.asset(
                'assets/graphics/icons/batman.png',
                fit: BoxFit.cover,
                height: 100,
                width: 100,
              )),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 16.0, right: 16, top: 8, bottom: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: Text(
                          blogNew!.title!,
                          maxLines: 3,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: UIColors.black),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "17 Oct 2020",
                              maxLines: 1,
                              softWrap: true,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontWeight: FontWeight.normal,
                                  fontSize: 10,
                                  color: UIColors.thirdTextFontColor),
                            ),
                          ),
                          Spacer(
                            flex: 20,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.all(5),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(90.0)),
                                    color: UIColors.tabBarDefaultColor),
                                child: Row(children: <Widget>[
                                  Icon(
                                    Icons.add,
                                    color: UIColors.white,
                                    size: 10,
                                  ),
                                  Text(
                                    "Ver mas",
                                    style: TextStyle(
                                        fontWeight: FontWeight.normal,
                                        fontSize: 10,
                                        color: UIColors.white),
                                  ),
                                ]),
                              )
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
