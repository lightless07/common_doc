import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CurrentMeetTile extends StatelessWidget {
  const CurrentMeetTile(
      {Key? key,
      this.context,
      this.profileUrl,
      this.profileName,
      this.isAble,
      this.onClick})
      : super(key: key);
  final String? profileUrl;
  final String? profileName;
  final BuildContext? context;
  final bool? isAble;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16, top: 16),
      child: Card(
        elevation: 5.0,
        child: InkWell(
          splashColor: UIColors.blue.withAlpha(30),
          onTap: () {
            onClick!();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  flex: 4,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "Cardiología Cardiología Cardiología ",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.secondTextFontColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                      ),
                      Container(
                        child: Text(
                          profileName!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.secondTextFontColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 12,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16, right: 16, top: 5),
                      ),
                      Container(
                        child: Text(
                          profileName!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.mainTextFontColor,
                            fontWeight: FontWeight.normal,
                            fontSize: 10,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                      ),
                      Container(
                        child: Text(
                          profileName!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.mainTextFontColor,
                            fontWeight: FontWeight.normal,
                            fontSize: 10,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16, right: 16),
                      ),
                      Container(
                        child: Text(
                          profileName!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.mainTextFontColor,
                            fontWeight: FontWeight.normal,
                            fontSize: 10,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding:
                            EdgeInsets.only(left: 16, right: 16, bottom: 16),
                      ),
                    ],
                  )),
              Expanded(
                  flex: 2,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(16),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: isAble!
                                ? UIColors.white
                                : UIColors.calendarUnableTileBgColor,
                            border: Border.all(
                                width: 1.0,
                                color: isAble!
                                    ? UIColors.secondTextFontColor
                                    : UIColors.white),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Sab",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: isAble!
                                          ? UIColors.secondTextFontColor
                                          : UIColors.thirdTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                                SizedBox(height: 10),
                                Container(
                                  child: Text(
                                    "10",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: isAble!
                                          ? UIColors.secondTextFontColor
                                          : UIColors.thirdTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 26,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                                SizedBox(height: 10),
                                Container(
                                  child: Text(
                                    "Nov",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: isAble!
                                          ? UIColors.secondTextFontColor
                                          : UIColors.thirdTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ]),
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
