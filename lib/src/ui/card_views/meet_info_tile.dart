import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/buttons/blue_button.dart';
import 'package:common_ui/src/ui/custom_views/custom_container.dart';
import 'package:flutter/material.dart';

class MeetInfoTile extends StatelessWidget {
  const MeetInfoTile(
      {Key? key,
      this.context,
      this.margin,
      this.date,
      this.fee,
      this.locationAddress,
      this.locationOnClick})
      : super(key: key);
  final String? date;
  final String? fee;
  final EdgeInsets? margin;
  final String? locationAddress;
  final BuildContext? context;
  final Function? locationOnClick;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5.0,
      margin: margin,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: InkWell(
          //splashColor: UIColors.blue.withAlpha(30),
          onTap: () {
            locationOnClick!();
          },
          child: Container(
              margin: EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 20),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Expanded(
                          flex: 3,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(top: 10),
                                child: titleWithUnderline(
                                    "Información de la cita"),
                                alignment: Alignment.centerLeft,
                              ),
                              Container(
                                child: Text(
                                  "Fecha: $date",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  textDirection: TextDirection.ltr,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: UIColors.mainTextFontColor,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14,
                                  ),
                                ),
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(top: 10),
                              ),
                              Container(
                                child: Text(
                                  "Costo: $fee",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  textDirection: TextDirection.ltr,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: UIColors.mainTextFontColor,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14,
                                  ),
                                ),
                                alignment: Alignment.centerLeft,
                              ),
                              Container(
                                child: Text(
                                  "Ubicación: $locationAddress",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                  textDirection: TextDirection.ltr,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    color: UIColors.mainTextFontColor,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14,
                                  ),
                                ),
                                alignment: Alignment.centerLeft,
                              ),
                              Container(
                                width: 110,
                                height: 25,
                                margin: EdgeInsets.only(top: 5),
                                child: BlueButton(
                                  isDisabled: false,
                                  title: "Ver ubicación",
                                  onClick: () {
                                    locationOnClick!();
                                  },
                                ),
                              )
                            ],
                          )),
                    ],
                  ),
                ],
              ))),
    );
  }
}
