import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/buttons/blue_button.dart';
import 'package:common_ui/src/ui/custom_views/custom_container.dart';
import 'package:flutter/material.dart';

class ConsultingRoomTile extends StatelessWidget {
  const ConsultingRoomTile(
      {Key? key, this.context, this.profileUrl, this.profileName, this.onClick})
      : super(key: key);
  final String? profileUrl;
  final String? profileName;
  final BuildContext? context;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return InkWell(
        //splashColor: UIColors.blue.withAlpha(30),
        onTap: () {
          onClick!();
        },
        child: Container(
            padding: EdgeInsets.only(left: 32, right: 32),
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                        flex: 3,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: titleWithUnderline("Consultorio"),
                              alignment: Alignment.centerLeft,
                            ),
                            Container(
                              child: Text(
                                profileName!,
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                textDirection: TextDirection.ltr,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  color: UIColors.mainTextFontColor,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 16,
                                ),
                              ),
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(top: 10),
                            ),
                            Container(
                              child: Row(children: <Widget>[
                                Icon(
                                  Icons.location_on_outlined,
                                  size: 16,
                                  color: UIColors.black,
                                ),
                                Text("Mty | 3.6km",
                                    style: TextStyle(
                                      color: UIColors.mainTextFontColor,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 14,
                                    ))
                              ]),
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(top: 10),
                            ),
                          ],
                        )),
                    Expanded(
                        flex: 2,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  BlueButton(
                                    isDisabled: false,
                                    title: "Ver ubicación",
                                    onClick: () {},
                                  ),
                                ])
                          ],
                        )),
                  ],
                ),
                customDivider()
              ],
            )));
  }
}
