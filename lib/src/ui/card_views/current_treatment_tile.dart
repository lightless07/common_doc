import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class CurrentTreatmentTile extends StatelessWidget {
  const CurrentTreatmentTile(
      {Key? key, this.context, this.profileUrl, this.profileName, this.onClick})
      : super(key: key);
  final String? profileUrl;
  final String? profileName;
  final BuildContext? context;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 16, right: 16, top: 16),
      child: Card(
        elevation: 5.0,
        child: InkWell(
          splashColor: UIColors.blue.withAlpha(30),
          onTap: () {
            onClick!();
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "Cardiología Cardiología Cardiología ",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.secondTextFontColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 15,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16, right: 16, top: 16),
                      ),
                      Container(
                        child: Text(
                          profileName!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.secondTextFontColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16, right: 16, top: 5),
                      ),
                      Container(
                        child: Text(
                          profileName!,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            color: UIColors.mainTextFontColor,
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 16, right: 16, top: 10),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: 16, right: 16, top: 10, bottom: 16),
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1.0,
                              color: UIColors.secondTextFontColor,
                            ),
                            borderRadius:
                                BorderRadius.all(Radius.circular(30.0)),
                          ),
                          height: 20.0,
                          child: Container(
                            alignment: Alignment.center,
                            child: Text(
                              "Recurrente",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: UIColors.secondTextFontColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  )),
              Expanded(
                  flex: 1,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1.0, color: UIColors.blue),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Sab",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: UIColors.secondTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                                SizedBox(height: 10),
                                Container(
                                  child: Text(
                                    "10",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: UIColors.secondTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 26,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                                SizedBox(height: 10),
                                Container(
                                  child: Text(
                                    "Nov",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: UIColors.secondTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ]),
                        ),
                      ),
                      Image.asset(
                        'assets/images/next_arrow_icon.png',
                        height: 7,
                        width: 6,
                      ),
                      Container(
                        padding: EdgeInsets.all(10),
                        child: Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            border:
                                Border.all(width: 1.0, color: UIColors.blue),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10.0)),
                          ),
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  child: Text(
                                    "Sab",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: UIColors.secondTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 12,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                                SizedBox(height: 10),
                                Container(
                                  child: Text(
                                    "10",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: UIColors.secondTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 26,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                                SizedBox(height: 10),
                                Container(
                                  child: Text(
                                    "Nov",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    textDirection: TextDirection.ltr,
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      color: UIColors.secondTextFontColor,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 10,
                                    ),
                                  ),
                                  alignment: Alignment.center,
                                ),
                              ]),
                        ),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
