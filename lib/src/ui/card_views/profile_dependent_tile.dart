import 'package:flutter/material.dart';

class ProfileDependentTile extends StatelessWidget {
  const ProfileDependentTile(
      {Key? key, this.name, this.relationship, this.age, this.onClick})
      : super(key: key);

  final Function? onClick;
  final String? name;
  final String? relationship;
  final String? age;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onClick!(),
      child: containerButton(),
    );
  }
}

Widget containerButton() {
  return Container(
    height: 106,
    width: 166,
    decoration: BoxDecoration(
      image: DecorationImage(
        image: AssetImage(
          'assets/images/bg_family.png',
        ),
        fit: BoxFit.cover,
      ),
    ),
    child: Container(
        child: Column(
      children: <Widget>[
        Expanded(
          flex: 3,
          child: Text("Name"),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: EdgeInsets.only(left: 12),
            child: Image.asset(
              'assets/images/icon_family.png',
              height: 28,
              width: 30,
            ),
          ),
        ),
      ],
    )),
  );
}

EdgeInsets getPadding(bool isDoctorButton) {
  if (isDoctorButton) {
    return EdgeInsets.only(right: 12, bottom: 9);
  } else {
    return EdgeInsets.only(left: 12, bottom: 9);
  }
}
