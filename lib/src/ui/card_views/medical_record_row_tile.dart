import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:common_ui/src/ui/check_box/checkbox_custom.dart';
import 'package:common_ui/src/ui/custom_views/custom_container.dart';
import 'package:flutter/material.dart';

class MedicalRecordRowTile extends StatefulWidget {
  const MedicalRecordRowTile(
      {Key? key, this.title, this.isSelected, this.onClick})
      : super(key: key);

  final Function? onClick;
  final String? title;
  final bool? isSelected;

  @override
  MedicalRecordRowTileState createState() => MedicalRecordRowTileState();
}

class MedicalRecordRowTileState extends State<MedicalRecordRowTile> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.onClick!(!widget.isSelected!),
      child: containerButton(),
    );
  }

  Widget containerButton() {
    return Container(
        padding: EdgeInsets.only(left: 32, right: 32),
        child: Column(children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                    flex: 2,
                    child: Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.title!,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.normal,
                            color: UIColors.mainTextFontColor),
                      ),
                    )),
                Expanded(
                  child: Container(
                      alignment: Alignment.centerRight,
                      padding: EdgeInsets.only(left: 30),
                      child: CheckboxCustom(
                        isChecked: widget.isSelected,
                      )),
                  flex: 3,
                ),
              ]),
          customDivider(),
        ]));
  }
}
