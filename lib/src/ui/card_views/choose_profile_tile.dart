import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class ChooseProfileTile extends StatelessWidget {
  const ChooseProfileTile(
      {Key? key, this.context, this.profileUrl, this.profileName, this.onClick})
      : super(key: key);
  final String? profileUrl;
  final String? profileName;
  final BuildContext? context;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 50,
        child: Card(
          elevation: 5.0,
          margin: EdgeInsets.all(10),
          child: InkWell(
              splashColor: UIColors.blue.withAlpha(30),
              onTap: () {
                onClick!();
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(width: 10),
                      Container(
                        width: 36,
                        height: 36,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              const Radius.circular(100.0)),
                          image: DecorationImage(
                              image: NetworkImage(profileUrl!),
                              fit: BoxFit.cover),
                        ),
                      ),
                      const SizedBox(width: 5),
                      Expanded(
                        child: Text(
                          profileName!,
                          maxLines: 4,
                          overflow: TextOverflow.ellipsis,
                          textDirection: TextDirection.ltr,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 12,
                              color: UIColors.mainTextFontColor),
                        ),
                      ),
                      const SizedBox(width: 10),
                    ],
                  ),
                ],
              )),
        ));
  }
}
