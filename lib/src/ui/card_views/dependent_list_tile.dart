import 'package:common_ui/src/resources/ui_colors.dart';
import 'package:flutter/material.dart';

class DependentListTile extends StatelessWidget {
  const DependentListTile(
      {Key? key,
      this.index,
      this.context,
      this.fullName,
      this.relationship,
      this.age,
      this.onClick})
      : super(key: key);
  final int? index;
  final String? fullName;
  final String? relationship;
  final String? age;
  final BuildContext? context;
  final Function? onClick;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () => onClick!(index),
        child: Container(
          width: 166,
          height: 106,
          padding: EdgeInsets.all(3),
          decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'assets/images/bg_family.png',
                ),
                fit: BoxFit.cover,
              ),
              boxShadow: <BoxShadow>[
                /*BoxShadow(
              color: UIColors.grey,
              blurRadius: 5.0,
            ),*/
              ],
              borderRadius: BorderRadius.all(Radius.circular(5))),
          margin: EdgeInsets.only(left: 5, right: 5, top: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          margin: EdgeInsets.only(bottom: 5, left: 20),
                          child: Text(
                            "Roberto González Ruiz",
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15,
                                color: UIColors.white),
                          )),
                      Container(
                          margin: EdgeInsets.only(bottom: 16, left: 20),
                          child: Text(
                            "Hijo | 12 años",
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                            textDirection: TextDirection.ltr,
                            textAlign: TextAlign.end,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 10,
                                color: UIColors.white),
                          )),
                    ],
                  )),
              Expanded(
                  flex: 1,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          height: 30,
                          width: 28,
                          margin: EdgeInsets.only(right: 16, top: 16),
                          child: Image.asset(
                            'assets/images/meet_confirmed_icon.png',
                            fit: BoxFit.contain,
                          )),
                    ],
                  )),
            ],
          ),
        ));
  }
}
